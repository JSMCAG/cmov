package pt.ulisboa.tecnico.cmov.airdesk.service.remote.services;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ServiceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 11-May-15.
 */
public class ReceiveCommunicationService {
    SimWifiP2pSocket peerSocket;

    public ReceiveCommunicationService(SimWifiP2pSocket peerSocket) {
        this.peerSocket = peerSocket;
    }

    public void execute() {

        try {
            //Fetch command from peer
            //publishProgress("Fetching command from peer");
            ObjectInputStream inStream = new ObjectInputStream(peerSocket.getInputStream());
            Command command = (Command) inStream.readObject();

            //Execute requested command
            //publishProgress("Executing requested command");
            command.execute();

            //return executed command to peer
            //publishProgress("Sending response to peer");
            ObjectOutputStream outputStream = new ObjectOutputStream(peerSocket.getOutputStream());
            outputStream.writeObject(command);
            outputStream.flush();

            inStream.close();
            outputStream.close();
            peerSocket.close();
            //return null;

        } catch (ServiceException | IOException | ClassNotFoundException e) {
            throw new CommunicationException();
        }
    }
}
