package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.local;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserAlreadyInWorkspaceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserAlreadyRequestedWorkspaceAccessException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceDoesNotExistException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceIsPrivateException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.RequestWorkspaceAccessService;

/**
 * Created by Filipe Apolinário on 17-Apr-15.
 */
public class AskToJoinWorkspaceAsyncTask extends
        AsyncTask<String, Void, String> {

    /**
     * The context.
     */
    private Context context;

    /**
     * The service.
     */
    private RequestWorkspaceAccessService service;

    /**
     * The workspaces list.
     */
    private ArrayList<String> workspacesList;

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * The message.
     */
    private String message;

    /**
     * Instantiates a new fetch user workspaces async task.
     *
     * @param context  the context
     * @param activity the activity
     */
    public AskToJoinWorkspaceAsyncTask(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        this.service = new RequestWorkspaceAccessService(context);

    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the array list
     */
    @Override
    protected String doInBackground(String... params) {
        String wsName = params[0];
        String ownerEmail = params[1];
        String foreignEmail = params[2];
        try {
            service.run(wsName, ownerEmail, foreignEmail);
            return "Successfully requested to join workspace" + "\"" + wsName + "\".";
        } catch (EmptyFieldsException e) {
            return "Some fields were empty.";
        } catch (WorkspaceIsPrivateException e) {
            return "Can not join a private workspace!";
        } catch (WorkspaceDoesNotExistException e) {
            return "Error: Workspace does not exist!";
        } catch (UserAlreadyRequestedWorkspaceAccessException e) {
            return "You already have requested workspace access.";
        } catch (UserAlreadyInWorkspaceException e) {
            return "You already have workspace access.";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, s, duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }
}
