package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.settings;

/**
 * Created by Group 7 on 22-Mar-15.
 */

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceAccessService;


/**
 * The Class ViewPeopleWorkspaceAccessAsyncTask.
 * Fetches the Users in Workspace.
 */
public class ViewUserWorkspaceAccessAsyncTask extends
        AsyncTask<String, Void, ArrayList<WorkspaceAccess>> {

    /**
     * The service.
     */
    private FetchWorkspaceAccessService service;

    /**
     * The activity.
     */
    private ActionBarActivity activity;

    /**
     * The name.
     */
    private String name;

    /**
     * The email.
     */
    private String email;

    /**
     * Instantiates a new view people workspace access async task.
     *
     * @param context  the context
     * @param activity the activity
     */
    public ViewUserWorkspaceAccessAsyncTask(Context context, ActionBarActivity activity) {
        super();
        this.service = new FetchWorkspaceAccessService(context);
        this.activity = activity;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the array list
     */
    @Override
    protected ArrayList<WorkspaceAccess> doInBackground(String... params) {
        this.name = params[0];
        this.email = params[1];

        service.run(name, email);

        return service.getResult();
    }

    /**
     * On post execute.
     *
     * @param wsList the ws list
     */
    @Override
    protected void onPostExecute(ArrayList<WorkspaceAccess> wsList) {
        ArrayList<String> list = new ArrayList<>();
        for (WorkspaceAccess ws : wsList) {
            list.add(ws.getEmailWithWorkspaceAccess());
        }

        // Create an instance of the dialog fragment and show it
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.dialog_settings_view_people_dialog);
        dialog.setTitle("Users with Workspace Access:");

        final ListView listView = (ListView) dialog.findViewById(R.id.viewPeopleListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_list_item_1, list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                final int position = listView.getPositionForView(myView);
                String user = listView.getItemAtPosition(position).toString();
                Bundle args = new Bundle();
                args.putString("workspace", name);
                args.putString("person", user);
                removePersonFromWorkspaceDialog(args);
                dialog.dismiss();
            }
        });
        listView.setAdapter(adapter);

        dialog.show();
    }

    /**
     * Removes the person from workspace dialog.
     *
     * @param args the args
     */
    private void removePersonFromWorkspaceDialog(Bundle args) {
        //TODO implement Dialog
        RemovePersonFromWorkspaceDialog dialog = new RemovePersonFromWorkspaceDialog();
        dialog.setArguments(args);
        dialog.show(activity.getSupportFragmentManager(), "NoticeDialogFragment");
    }

}
