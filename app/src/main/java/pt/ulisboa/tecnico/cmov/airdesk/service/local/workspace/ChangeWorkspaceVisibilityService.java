package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

/**
 * Created by Group 7 on 31-Mar-15.
 */

import android.content.Context;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;

/**
 * The Class ChangeWorkspaceVisibilityService.
 */
public class ChangeWorkspaceVisibilityService {

    /**
     * The database my workspace handler.
     */
    private WorkspaceDataSource databaseMyWorkspaceHandler;

    /**
     * Instantiates a new change workspace visibility service.
     *
     * @param context the context
     */
    public ChangeWorkspaceVisibilityService(Context context) {
        databaseMyWorkspaceHandler = new WorkspaceDataSource(context);
    }

    /**
     * Run.
     *
     * @param name               the name
     * @param ownerEmail         the owner email
     * @param isPrivateWorkspace the is private workspace
     */
    public void run(String name, String ownerEmail, boolean isPrivateWorkspace) {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {name, ownerEmail};
        if (!verifier.stringVerifier(sParams)) {
            return;
        }

        databaseMyWorkspaceHandler.open();

        Workspace ws = databaseMyWorkspaceHandler.findWorkspaceByNameAndOwner(name, ownerEmail);
        if (ws != null) {
            ws.setPrivateWorkspace(isPrivateWorkspace);
            databaseMyWorkspaceHandler.changeWorkspace(ws);
        }

        databaseMyWorkspaceHandler.close();
    }
}
