package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files.task.remote;

/**
 * Created by Group 7 on 07-Apr-15.
 */

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.fileCommands.DeleteFileCommand;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.RequestCommunicationService;

/**
 * The Class DeleteFileAsyncTask.
 * this task deletes the file from the FileStorage.
 */
public class DeleteFileAsyncTask extends
        AsyncTask<P2PNode, String, Void> {

    /**
     * The file name.
     */
    private String fileName;

    /**
     * The owner email.
     */
    private String ownerEmail;
    /**
     * The workspace name.
     */
    private String workspaceName;

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * The message.
     */
    private String message;

    /**
     * The context.
     */
    private Context context;

    /**
     * Instantiates a new delete file async task.
     *
     * @param c        the c
     * @param activity the activity
     * @param fileName the file name
     */
    public DeleteFileAsyncTask(Context c,
                               Activity activity,
                               String workspaceName,
                               String ownerEmail,
                               String fileName) {
        super();
        this.fileName = fileName;
        this.activity = activity;
        this.context = c;
        this.workspaceName = workspaceName;
        this.ownerEmail = ownerEmail;
    }

    /**
     * On pre execute.
     */
    @Override
    protected void onPreExecute() {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, "Deleting file: Please wait.", duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(P2PNode... params) {
        ArrayList<Workspace> workspaceList = new ArrayList<>();
        for (P2PNode peer : params) {
            if (peer.getOwnerEmail().equals(ownerEmail)) {
                try {
                    DeleteFileCommand command = new DeleteFileCommand(workspaceName, fileName);

                    //send request to peer
                    publishProgress("Sending request to peer");
                    RequestCommunicationService service = new RequestCommunicationService(command, peer, context);
                    service.execute();


                    DeleteFileCommand response = (DeleteFileCommand) service.getResult();
                    publishProgress(response.getResult());

                } catch (CommunicationException e) {
                    publishProgress("Could not delete file");
                }
            }
        }
        return null;
    }


    @Override
    protected void onProgressUpdate(String... values) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, values[0], duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    /**
     * On post execute.
     *
     * @param aVoid the a void
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        reloadActivity(this.activity);
    }

    /**
     * Reload activity.
     *
     * @param activity the activity
     */
    private void reloadActivity(Activity activity) {
        activity.finish();
        activity.startActivity(activity.getIntent());
    }
}
