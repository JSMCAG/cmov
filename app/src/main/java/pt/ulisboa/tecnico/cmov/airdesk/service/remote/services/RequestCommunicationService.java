package pt.ulisboa.tecnico.cmov.airdesk.service.remote.services;

import android.content.Context;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 11-May-15.
 */
public class RequestCommunicationService {
    Command request = null;
    Command result = null;
    P2PNode peer;
    Context context;

    public RequestCommunicationService(Command request, P2PNode peer, Context context){
        this.request = request;
        this.peer = peer;
        this.context = context;
    }

    public void execute(){

        try{
            //create a socket for node connection
            SimWifiP2pSocket peerSocket = new SimWifiP2pSocket(peer.getVirtualIP(),
                            Integer.parseInt(context.getString(R.string.port)));

            //send command to peer
            //publishProgress("Sending response to peer");
            ObjectOutputStream outputStream = new ObjectOutputStream(peerSocket.getOutputStream());
            outputStream.writeObject(request);
            outputStream.flush();

            //Receive executed command from peer
            //publishProgress("Fetching command from peer");
            ObjectInputStream inStream = new ObjectInputStream(peerSocket.getInputStream());
            Command command = (Command) inStream.readObject();
            result = command;

            //close communication
            inStream.close();
            outputStream.close();
            peerSocket.close();

        } catch(IOException | ClassNotFoundException e){
            throw new CommunicationException();
        }
    }

    public Command getResult(){
        return result;
    }
}
