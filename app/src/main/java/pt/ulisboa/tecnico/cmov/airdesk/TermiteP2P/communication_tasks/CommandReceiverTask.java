package pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.communication_tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.ReceiveCommunicationService;

/**
 * Created by Filipe Apolinário on 10-May-15.
 */
public class CommandReceiverTask extends AsyncTask<SimWifiP2pSocket, String, String> {

    public static final String TAG = "simplechat";
    SimWifiP2pSocket s;
    private Context context;

    public CommandReceiverTask(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(SimWifiP2pSocket... params) {
        try {
            SimWifiP2pSocket peerSocket = params[0];

            //Fetch command from peer
            publishProgress("Fetching command from peer");
            new ReceiveCommunicationService(peerSocket).execute();
            return "successfully executed command and sent response to peer";

        } catch (CommunicationException e) {
            return "could not perform command";
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        Toast toast = Toast.makeText(context, values[0], Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    protected void onPostExecute(String result) {
        if (result != null) {
            Toast toast = Toast.makeText(context, result, Toast.LENGTH_SHORT);
            toast.show();
        }
    }


}
