package pt.ulisboa.tecnico.cmov.airdesk;

/**
 * Created by Group 7 on 13-Mar-15.
 */

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import java.io.IOException;

import pt.inesc.termite.wifidirect.service.SimWifiP2pService;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.WifiDirectStateKeeper;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.User;
//import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.SimpleChatActivity;

/**
 * The Class SimpleApp.
 */
public class SimpleApp extends
        Application {

    /**
     * The user logged in.
     */
    private static User userLoggedIn = null;

    private static WifiDirectStateKeeper wifiDirectStateKeeper = null;

    private static Context context;

    /**
     * Gets the user logged in.
     *
     * @return the user logged in
     */

    public static User getUserLoggedIn() {
        return userLoggedIn;
    }

    /**
     * Sets the user logged in.
     *
     * @param user the new user logged in
     */
    public static void setUserLoggedIn(User user) {
        userLoggedIn = user;
    }

    public static WifiDirectStateKeeper getWifiDirectStateKeeper() {
        return wifiDirectStateKeeper;
    }

    public static Context getContext() {
        return context;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        if (SimpleApp.getWifiDirectStateKeeper().isWifiEnabled()) {
            this.getApplicationContext().unbindService(SimpleApp.getWifiDirectStateKeeper().getmConnection());
            //activity.mBound = false;
            try {
                wifiDirectStateKeeper.getmSrvSocket().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();

        wifiDirectStateKeeper = new WifiDirectStateKeeper(getApplicationContext());

        Intent intent = new Intent(getApplicationContext(), SimWifiP2pService.class);
        getApplicationContext().bindService(intent, SimpleApp.getWifiDirectStateKeeper().getmConnection(), Context.BIND_AUTO_CREATE);


    }
}
