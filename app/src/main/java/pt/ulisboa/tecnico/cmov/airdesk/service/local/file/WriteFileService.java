package pt.ulisboa.tecnico.cmov.airdesk.service.local.file;

/**
 * Created by Group 7 on 04-Apr-15.
 */

import android.content.Context;

import java.io.File;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.FileStorageHandler;

/**
 * The Class WriteFileService.
 * This service writes file to FileStorage.
 */
public class WriteFileService {

    /**
     * The file directory.
     */
    private File fileDirectory;

    /**
     * The result.
     */
    private boolean result;

    /**
     * Instantiates a new write file service.
     *
     * @param context the context
     */
    public WriteFileService(Context context) {
        this.fileDirectory = context.getFilesDir();
    }

    /**
     * Run.
     *
     * @param workspaceRelativePath the workspace relative path
     * @param fileName              the file name
     * @param text                  the text
     * @param quota                 the quota
     */
    public void run(String workspaceRelativePath, String fileName, String text, int quota) {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {workspaceRelativePath, fileName, text};
        if (!verifier.stringVerifier(sParams)) {
            return;
        }
        int[] iParams = {quota};
        if (!verifier.stringVerifier(sParams) || !verifier.positiveVerifier(iParams)) {
            return;
        }

        String absolutePath = fileDirectory.getAbsolutePath();
        FileStorageHandler handler = new FileStorageHandler();
        String wsAbsPath = absolutePath + File.separator + workspaceRelativePath;
        if (!handler.doesDirectoryExist(wsAbsPath)) {
            handler.createDirectory(wsAbsPath);
        }

        if (handler.canWriteInWorkspace(
                (int) ((long) text.getBytes().length), quota, absolutePath)) {
            handler.write(wsAbsPath + File.separator + fileName, text);
            this.result = true;
        } else
            this.result = false;
    }

    /**
     * Gets the result.
     *
     * @return the result
     */
    public boolean getResult() {
        return this.result;
    }
}
