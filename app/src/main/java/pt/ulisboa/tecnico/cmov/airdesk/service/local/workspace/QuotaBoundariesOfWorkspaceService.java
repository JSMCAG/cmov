/*
 * 
 */
package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;


/**
 * Created by Group 7 on 08-Apr-15.
 */

import android.content.Context;

import java.io.File;

import pt.ulisboa.tecnico.cmov.airdesk.core.FileStorageHandler;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;

/**
 * The Class QuotaBoundariesOfWorkspaceService.
 */
public class QuotaBoundariesOfWorkspaceService {


    /**
     * The result.
     */
    private long[] result = new long[2];

    /**
     * The files dir.
     */
    private File filesDir;

    {
        result[0] = 0;
        result[1] = 1;
    }

    /**
     * Instantiates a new quota boundaries of workspace service.
     *
     * @param context the context
     */
    public QuotaBoundariesOfWorkspaceService(Context context) {
        filesDir = context.getFilesDir();
    }

    /**
     * Run.
     *
     * @param ws the ws
     */
    public void run(Workspace ws) {
        if (ws != null) {
            String absolutePath = filesDir.getAbsolutePath();
            absolutePath += File.separator + ws.getDirectory();
            FileStorageHandler handler = new FileStorageHandler();
            result[0] = handler.byteToMegaByte(handler.getUsedSpaceAmount(absolutePath));
        }
        FileStorageHandler handler = new FileStorageHandler();
        this.result[1] = handler.byteToMegaByte(handler.getFreeSpaceAmount(filesDir.toString()));
    }


    /**
     * Gets the result.
     *
     * @return the result
     */
    public long[] getResult() {
        return this.result;
    }
}
