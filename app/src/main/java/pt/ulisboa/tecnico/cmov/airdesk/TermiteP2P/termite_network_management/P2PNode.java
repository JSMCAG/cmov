package pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management;

/**
 * Created by Filipe Apolinário on 09-May-15.
 */
public class P2PNode {
    private String deviceName;
    private String virtualIP;
    private String port;
    private String ownerEmail = null;

    public P2PNode(String deviceName, String virtualIP, String port) {
        this.deviceName = deviceName;
        this.virtualIP = virtualIP;
        this.port = port;
    }

    public P2PNode(String deviceName, String virtualIP) {
        this.deviceName = deviceName;
        this.virtualIP = virtualIP;
    }

    @Override
    public String toString() {
        return "[" + this.deviceName + ":" + this.virtualIP + "]";
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getVirtualIP() {
        return virtualIP;
    }

    public void setVirtualIP(String virtualIP) {
        this.virtualIP = virtualIP;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }
}
