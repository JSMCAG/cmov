package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.settings;

/**
 * Created by Group 7 on 08-Apr-15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserNotInWorkspaceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.LeaveWorkspaceService;

/**
 * The Class RemovePersonFromWorkspaceAsyncTask.
 * Removes User from Workspace.
 */
public class RemovePersonFromWorkspaceAsyncTask extends
        AsyncTask<String, Void, Void> {

    /**
     * The service.
     */
    private LeaveWorkspaceService service;

    /**
     * The message.
     */
    private String message;

    /**
     * The context.
     */
    private Context context;

    /**
     * The name.
     */
    private String name;

    /**
     * The owner email.
     */
    private String ownerEmail;

    /**
     * The foreign email.
     */
    private String foreignEmail;

    /**
     * Instantiates a new removes the person from workspace async task.
     *
     * @param context      the context
     * @param name         the name
     * @param ownerEmail   the owner email
     * @param foreignEmail the foreign email
     */
    public RemovePersonFromWorkspaceAsyncTask(Context context,
                                              String name,
                                              String ownerEmail,
                                              String foreignEmail) {
        super();
        service = new LeaveWorkspaceService(context);
        this.context = context;
        this.name = name;
        this.ownerEmail = ownerEmail;
        this.foreignEmail = foreignEmail;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(String... params) {

        try {
            service.run(name, ownerEmail, foreignEmail);
            this.message = "Successfully removed User from Workspace.";
        } catch (EmptyFieldsException e) {
            this.message = "Some fields were empty.";
        } catch (UserNotInWorkspaceException e) {
            this.message = "User hasn't access to Workspace.";
        }

        return null;
    }

    /**
     * On post execute.
     *
     * @param aVoid the a void
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }
}
