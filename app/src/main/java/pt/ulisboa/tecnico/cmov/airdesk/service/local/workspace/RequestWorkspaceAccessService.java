package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

/**
 * Created by Filipe Apolinário on 17-Apr-15.
 */

import android.content.Context;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.GrantWorkspaceAccessDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceAccessDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserAlreadyInWorkspaceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserAlreadyRequestedWorkspaceAccessException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceDoesNotExistException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceIsPrivateException;

/**
 * The Class AddPersonToWorkspaceAccessService.
 */
public class RequestWorkspaceAccessService {

    /**
     * The database people with workspace access handler.
     */
    private WorkspaceAccessDataSource databasePeopleWithWorkspaceAccessHandler;

    /**
     * The database people waiting for workspace access handler.
     */
    private GrantWorkspaceAccessDataSource databaseRequestWorkspaceAccessHandler;

    /**
     * The context.
     */
    private Context context;

    /**
     * Instantiates a new adds the person to workspace access service.
     *
     * @param context the context
     */
    public RequestWorkspaceAccessService(Context context) {
        this.context = context;
        databaseRequestWorkspaceAccessHandler = new GrantWorkspaceAccessDataSource(context);
        databasePeopleWithWorkspaceAccessHandler = new WorkspaceAccessDataSource(context);
    }

    /**
     * Run.
     *
     * @param wsName       the ws name
     * @param wsOwnerEmail the ws owner email
     * @param foreignEmail the foreign email
     */
    public void run(String wsName, String wsOwnerEmail, String foreignEmail) {
        FieldVerifier verifier = new FieldVerifier();
        String[] params = {wsName, wsOwnerEmail, foreignEmail};
        if (!verifier.stringVerifier(params)) {
            throw new EmptyFieldsException();
        }
        //Verify workspace exists and if it is a public Workspace.
        FetchWorkspaceService service = new FetchWorkspaceService(context);
        service.run(wsOwnerEmail, wsName);
        Workspace ws = service.getResult();
        if (ws == null) {
            throw new WorkspaceDoesNotExistException();
        }
        if (ws.isPrivateWorkspace()) {
            throw new WorkspaceIsPrivateException();
        }
        databasePeopleWithWorkspaceAccessHandler.open();
        if (databasePeopleWithWorkspaceAccessHandler.isForeignInWorkspace(wsName, wsOwnerEmail, foreignEmail)) {
            databasePeopleWithWorkspaceAccessHandler.close();
            throw new UserAlreadyInWorkspaceException();
        }
        databasePeopleWithWorkspaceAccessHandler.close();
        databaseRequestWorkspaceAccessHandler.open();
        if (databaseRequestWorkspaceAccessHandler.isForeignInWorkspaceRequest(wsName, wsOwnerEmail, foreignEmail)) {
            databaseRequestWorkspaceAccessHandler.close();
            throw new UserAlreadyRequestedWorkspaceAccessException();
        }

        //Ok lets add a person to the database!
        WorkspaceAccess wsA = new WorkspaceAccess(wsName, wsOwnerEmail, foreignEmail);
        databaseRequestWorkspaceAccessHandler.addWorkspaceRequestAccess(wsA);
        databaseRequestWorkspaceAccessHandler.close();
    }
}