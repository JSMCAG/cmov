package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files;

/**
 * Created by Group 7 on 26-Mar-15.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files.task.local.ReadFileAsyncTask;

/**
 * The Class EditFileActivity.
 * activity that reads the file and lets the user edit the file.
 */
public class EditMyWorkspaceFileActivity extends
        ActionBarActivity {

    /**
     * The file name.
     */
    private String FILE_NAME;

    /**
     * The folder abs path.
     */
    private String FOLDER_ABS_PATH;

    /**
     * The quota.
     */
    private int QUOTA;

    /**
     * On create.
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_file);
        Intent intent = getIntent();
        FILE_NAME = intent.getStringExtra("file_name");
        FOLDER_ABS_PATH = intent.getStringExtra("folder_abs_path");
        QUOTA = intent.getIntExtra("quota", 0);
        initializeLayoutReadFile(FILE_NAME);
    }

    /**
     * Initialize layout read file.
     *
     * @param file_name the file_name
     */
    private void initializeLayoutReadFile(String file_name) {
        TextView readFileTitleText = (TextView) findViewById(R.id.editFileLabel);
        readFileTitleText.setText(file_name);

        TextView readFileContentText = (TextView) findViewById(R.id.editFileContent);
        ReadFileAsyncTask task = new ReadFileAsyncTask(getApplicationContext(),
                FOLDER_ABS_PATH, FILE_NAME, readFileContentText);
        task.execute();
    }

    /**
     * On create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_write_file, menu);
        return true;
    }

    /**
     * On options item selected.
     *
     * @param item the item
     * @return true, if successful
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Launch finish confirmation dialog edit file activity.
     *
     * @param v the v
     */
    public void launchFinishConfirmationDialogEditFileActivity(View v) {
        // Create an instance of the dialog fragment and show it
        EditMyWorkspaceFileFinishConfirmationDialog dialog = new EditMyWorkspaceFileFinishConfirmationDialog();
        Bundle bundle = new Bundle();
        bundle.putString("file_name", FILE_NAME);
        bundle.putString("folder_abs_path", FOLDER_ABS_PATH);
        bundle.putInt("quota", QUOTA);
        TextView readFileContentText = (TextView) findViewById(R.id.editFileContent);
        String text = readFileContentText.getText().toString();
        bundle.putString("body", text);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
    }
}
