package pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management;

import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.Messenger;
import android.widget.Toast;

import java.util.ArrayList;

import pt.inesc.termite.wifidirect.SimWifiP2pBroadcast;
import pt.inesc.termite.wifidirect.SimWifiP2pDeviceList;
import pt.inesc.termite.wifidirect.SimWifiP2pInfo;
import pt.inesc.termite.wifidirect.SimWifiP2pManager;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocketManager;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocketServer;
import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.communication_tasks.RequestEmailTask;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.ExchangeEmailCommand;

/**
 * Created by Filipe Apolinário on 09-May-15.
 */
public class WifiDirectStateKeeper implements SimWifiP2pManager.GroupInfoListener {


    //WIFI DIRECT

    private boolean wifiEnabled = false;
    private SimWifiP2pDeviceList peerList = null;
    private SimWifiP2pInfo simWifiP2pInfo = null;
    private ArrayList<P2PNode> connectedPeersList = new ArrayList<>();
    private ArrayList<SimWifiP2pSocket> socketArrayList = new ArrayList<>();
    private SimWifiP2pSocketServer mSrvSocket = null;
    private ServiceConnection mConnection;
    private String deviceName = null;
    //(not sure if the following parameters are needed
    private SimWifiP2pManager mManager = null;
    private SimWifiP2pManager.Channel mChannel = null;
    private Messenger mService = null;

    public WifiDirectStateKeeper(final Context context) {

        initializerBindServiceCallbacks(context);

        // initialize the WDSim API
        SimWifiP2pSocketManager.Init(context);

        // register broadcast receiver
        registerBroadcastReceiver(context);

    }

    public SimWifiP2pManager getmManager() {
        return mManager;
    }

    public SimWifiP2pManager.Channel getmChannel() {
        return mChannel;
    }

    private void initializerBindServiceCallbacks(final Context context) {
        mConnection = new ServiceConnection() {
            // callbacks for service binding, passed to bindService()

            @Override
            public void onServiceConnected(ComponentName className, IBinder service) {
                mService = new Messenger(service);
                mManager = new SimWifiP2pManager(mService);
                mChannel = mManager.initialize(context, context.getMainLooper(), null);
                wifiEnabled = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName arg0) {
                mService = null;
                mManager = null;
                mChannel = null;
                wifiEnabled = false;
            }
        };
    }

    public void registerBroadcastReceiver(Context context) {
        //intent filter notifications
        IntentFilter filter = new IntentFilter();
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_STATE_CHANGED_ACTION);
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_PEERS_CHANGED_ACTION);
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_NETWORK_MEMBERSHIP_CHANGED_ACTION);
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_GROUP_OWNERSHIP_CHANGED_ACTION);
        //register receiver with notifications
        SimWifiP2pBroadcastReceiver receiver = new SimWifiP2pBroadcastReceiver(context);
        context.registerReceiver(receiver, filter);
    }


    //Getters and Setters


    public boolean isWifiEnabled() {
        return wifiEnabled;
    }

    public void setWifiEnabled(boolean wifiEnabled) {
        this.wifiEnabled = wifiEnabled;
    }

    public SimWifiP2pDeviceList getPeerList() {
        return peerList;
    }

    public void setPeerList(SimWifiP2pDeviceList peerList) {
        this.peerList = peerList;
    }

    public SimWifiP2pInfo getSimWifiP2pInfo() {
        return simWifiP2pInfo;
    }

    public void setSimWifiP2pInfo(SimWifiP2pInfo simWifiP2pInfo) {
        this.simWifiP2pInfo = simWifiP2pInfo;
    }

    public ArrayList<P2PNode> getConnectedPeersList() {
        return connectedPeersList;
    }

    public void setConnectedPeersList(ArrayList<P2PNode> connectedPeersList) {
        this.connectedPeersList = connectedPeersList;
    }

    public void registerConnectedPeer(P2PNode node) {
        connectedPeersList.add(node);
    }

    public ArrayList<SimWifiP2pSocket> getSocketArrayList() {
        return socketArrayList;
    }

    public void setSocketArrayList(ArrayList<SimWifiP2pSocket> socketArrayList) {
        this.socketArrayList = socketArrayList;
    }

    public SimWifiP2pSocketServer getmSrvSocket() {
        return mSrvSocket;
    }

    public void setmSrvSocket(SimWifiP2pSocketServer mSrvSocket) {
        this.mSrvSocket = mSrvSocket;
    }

    public ServiceConnection getmConnection() {
        return mConnection;
    }

    public void setmConnection(ServiceConnection mConnection) {
        this.mConnection = mConnection;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }


    @Override
    public void onGroupInfoAvailable(SimWifiP2pDeviceList devices,
                                     SimWifiP2pInfo groupInfo) {

        groupInfo.print();
        SimpleApp.getWifiDirectStateKeeper().setDeviceName(groupInfo.getDeviceName());
        SimpleApp.getWifiDirectStateKeeper().setSimWifiP2pInfo(groupInfo);
        SimpleApp.getWifiDirectStateKeeper().setConnectedPeersList(new ArrayList<P2PNode>());
        for (String name : groupInfo.getDevicesInNetwork()) {
            String virtualIP = devices.getByName(name).getVirtIp();
            SimpleApp.getWifiDirectStateKeeper().registerConnectedPeer(new P2PNode(name, virtualIP));
        }
        String myEmail = null;
        Toast.makeText(SimpleApp.getContext(), "New Network membership is:\n" + SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList().toString(),
                Toast.LENGTH_SHORT).show();
        ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
        if (SimpleApp.getUserLoggedIn() != null) {
            myEmail = SimpleApp.getUserLoggedIn().getEmail();
        }
        ExchangeEmailCommand command = new ExchangeEmailCommand(myEmail, SimpleApp.getWifiDirectStateKeeper().getDeviceName());

        for (P2PNode peer : peerList) {
            new RequestEmailTask(SimpleApp.getContext(), command).executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, peer);
        }
    }
}
