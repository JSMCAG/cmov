package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.FileDto;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceInstallObject;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.ReadFileService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.SearchFilesInWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceAccessService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 15-May-15.
 */
public class DownloadWorkspaceCommand extends Command {
    private String workspaceName;

    private WorkspaceInstallObject result;

    public DownloadWorkspaceCommand(String workspaceName) {
        this.workspaceName = workspaceName;
    }

    public void execute() {
        FetchWorkspaceService service = new FetchWorkspaceService(SimpleApp.getContext());
        service.run(SimpleApp.getUserLoggedIn().getEmail(), workspaceName);
        Workspace ws = service.getResult();

        FetchWorkspaceAccessService fetchWorkspaceAccessService = new FetchWorkspaceAccessService(SimpleApp.getContext());
        fetchWorkspaceAccessService.run(workspaceName, SimpleApp.getUserLoggedIn().getEmail());
        ArrayList<WorkspaceAccess> accesses = fetchWorkspaceAccessService.getResult();

        SearchFilesInWorkspaceService filesInWorkspaceService = new SearchFilesInWorkspaceService(SimpleApp.getContext(), ws.getDirectory());
        filesInWorkspaceService.run();
        ArrayList<String> fileNames = filesInWorkspaceService.getResult();

        ArrayList<FileDto> fileDtos = new ArrayList<>();
        for (String name : fileNames) {
            ReadFileService readFileService = new ReadFileService(SimpleApp.getContext());
            readFileService.run(ws.getDirectory(), name);
            String fileContent = readFileService.getResult();

            fileDtos.add(new FileDto(name, fileContent));
        }

        result = new WorkspaceInstallObject(ws, accesses, fileDtos);

    }

    public WorkspaceInstallObject getResult() {
        return result;
    }
}