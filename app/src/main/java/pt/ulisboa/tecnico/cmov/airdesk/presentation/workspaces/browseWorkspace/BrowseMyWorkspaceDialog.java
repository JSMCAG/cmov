package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace;

/**
 * Created by Group 7 on 16-Mar-15.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.ListView;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files.EditMyWorkspaceFileActivity;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files.ReadMyWorkspaceFileActivity;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files.task.local.DeleteFileAsyncTask;

/**
 * The Class BrowseWorkspaceDialog.
 * This Dialog lists all the possible operations to a file and lets the user pick one.
 */
public class BrowseMyWorkspaceDialog extends
        DialogFragment {

    /**
     * On create dialog.
     *
     * @param savedInstanceState the saved instance state
     * @return the dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String FILE_NAME = getArguments().getString("title");
        final String FOLDER_ABS_PATH = getArguments().getString("folder_abs_path");
        final int QUOTA = getArguments().getInt("quota");
        builder.setTitle(FILE_NAME);
        builder.setItems(R.array.browse_workspace_dialog_array,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item

                        String valueOptionPressed = getMyWorkspaceOptionPressed((Dialog) dialog,
                                which);

                        final String READ_FILE = getResources().getText(R.string.dialog_read_file)
                                .toString();
                        final String EDIT_FILE = getResources().getText(R.string.dialog_edit_file)
                                .toString();
                        final String DELETE_FILE = getResources().getText(
                                R.string.dialog_delete_file).toString();

                        if (valueOptionPressed.equals(READ_FILE)) {
                            Intent intent = new Intent(getActivity(), ReadMyWorkspaceFileActivity.class);
                            intent.putExtra("file_name", FILE_NAME);
                            intent.putExtra("folder_abs_path", FOLDER_ABS_PATH);
                            startActivity(intent);
                        } else if (valueOptionPressed.equals(EDIT_FILE)) {
                            Intent intent = new Intent(getActivity(), EditMyWorkspaceFileActivity.class);
                            intent.putExtra("file_name", FILE_NAME);
                            intent.putExtra("folder_abs_path", FOLDER_ABS_PATH);
                            intent.putExtra("quota", QUOTA);
                            startActivity(intent);
                        } else if (valueOptionPressed.equals(DELETE_FILE)) {
                            //TODO: add dialog to confirm delete
                            //This seems to be a dangerous option, there should be some confirmation before executing any operation.
                            //I suggest another dialog to test if the user really wants to leave the workspace.
                            Activity activity = getActivity();
                            DeleteFileAsyncTask task = new DeleteFileAsyncTask(activity
                                    .getApplicationContext(), activity, FOLDER_ABS_PATH, FILE_NAME);
                            task.execute();
                        }
                    }

                    private String getMyWorkspaceOptionPressed(Dialog dialog, int which) {
                        ListView v = (ListView) dialog.getCurrentFocus();
                        return v.getItemAtPosition(which).toString();
                    }
                });

        // Create the AlertDialog object and return it
        return builder.create();
    }

}
