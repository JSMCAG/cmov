package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

/**
 * Created by Group 7 on 07-Apr-15.
 */

import android.content.Context;

import java.io.File;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.FileStorageHandler;

/**
 * The Class CheckQuotaService.
 * This service updates the workspace's quota on the database.
 */
public class CheckQuotaService {

    /**
     * The file directory.
     */
    private File fileDirectory;

    /**
     * The result.
     */
    private boolean result = false;

    /**
     * Instantiates a new check quota service.
     *
     * @param context the context
     */
    public CheckQuotaService(Context context) {
        this.fileDirectory = context.getFilesDir();
    }

    /**
     * Run.
     *
     * @param wsRelativePath the ws relative path
     * @param quota          the quota
     */
    public void run(String wsRelativePath, int quota) {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {wsRelativePath};
        int[] iParams = {quota};
        if (!verifier.stringVerifier(sParams) || !verifier.positiveVerifier(iParams)) {
            return;
        }

        String absolutePath = fileDirectory.getAbsolutePath();
        String wsAbsPath = absolutePath + File.separator + wsRelativePath;

        FileStorageHandler handler = new FileStorageHandler();
        if (!handler.doesDirectoryExist(wsAbsPath)) {
            handler.createDirectory(wsAbsPath);
        }

        this.result = handler.isQuotaValid(quota, wsAbsPath);
    }


    /**
     * Gets the result.
     *
     * @return the result
     */
    public boolean getResult() {
        return this.result;
    }
}
