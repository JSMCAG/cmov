package pt.ulisboa.tecnico.cmov.airdesk.presentation.login;

/**
 * Created by Group 7 on 26-Mar-15.
 */

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

import pt.inesc.termite.wifidirect.service.SimWifiP2pService;
import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.communication_tasks.IncomingCommTask;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.WifiDirectStateKeeper;

/**
 * The Class LoginActivity.
 * This activity logs user
 */
public class LoginActivity extends
        ActionBarActivity {
    public void testeWifiDirect(View v) {
/*
        ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
        P2PNode[] peerArray = new P2PNode[peerList.size()];
        peerArray = peerList.toArray(peerArray);
        ExchangeEmailCommand command = new ExchangeEmailCommand();
        Context context = getApplicationContext();

        for(P2PNode peer : peerList) {
            new RequestEmailTask(context, command).executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, peer);
        }*/
    }

    public void wiFiOn(View v) {
        if (!SimpleApp.getWifiDirectStateKeeper().isWifiEnabled()) {
            Intent intent = new Intent(v.getContext(), SimWifiP2pService.class);
            WifiDirectStateKeeper keeper = SimpleApp.getWifiDirectStateKeeper();
            this.getApplicationContext().bindService(intent, SimpleApp.getWifiDirectStateKeeper().getmConnection(), Context.BIND_AUTO_CREATE);

            new IncomingCommTask(getApplicationContext()).executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR);
        }

    }

    public void wiFiOff(View v) {
        if (SimpleApp.getWifiDirectStateKeeper().isWifiEnabled()) {
            this.getApplicationContext().unbindService(SimpleApp.getWifiDirectStateKeeper().getmConnection());
            //activity.mBound = false;
            try {
                SimpleApp.getWifiDirectStateKeeper().getmSrvSocket().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * On create.
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }


    /**
     * On create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    /**
     * On options item selected.
     *
     * @param item the item
     * @return true, if successful
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Login.
     * This method starts when the user clicks on the login button
     * Performs validation of the email.
     * If the email is valid, it is then checked if the user already exists in the Database, if not it's created a new account.
     * After the login is performed AirDesk jumps to the newly logged user's MyWorkspacesActivity.
     *
     * @param v the v
     */
    public void login(View v) {
        //Method invoked when login button is pressed
        //this creates a background task that validates the user login
        TextView textView = (TextView) findViewById(R.id.emailText);
        String email = textView.getText().toString();
        LogInAsyncTask task = new LogInAsyncTask(this, getApplicationContext());
        task.execute(email);
    }


}
