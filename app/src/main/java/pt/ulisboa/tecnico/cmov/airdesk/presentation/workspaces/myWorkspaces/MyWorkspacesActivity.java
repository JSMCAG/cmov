package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces;

/**
 * Created by Group 7 on 26-Mar-15.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.ForeignWorkspacesActivity;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.QuotaBoundariesOfWorkspaceService;

/**
 * The Class MyWorkspacesActivity.
 * This activity lists all the user's Workspace, and lets User navigate to WorkspaceSettingsActivity and navigate to BrowseWorkspaceActivity.
 */
public class MyWorkspacesActivity extends
        ActionBarActivity {

    /**
     * The my workspaces.
     */
    protected ArrayList<Workspace> myWorkspaces;

    /**
     * On create.
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_workspaces);

        // for login testing porpoises.
        // When we are sure login works, these lines should be removed

        //Initialize user logged in text
        TextView textView = (TextView) findViewById(R.id.userEmail);
        textView.setText(SimpleApp.getUserLoggedIn().getEmail());

        //-Fetch WsList and initialize it
        FetchUserWorkspacesAsyncTask task = new FetchUserWorkspacesAsyncTask(
                getApplicationContext(), this);
        task.execute(SimpleApp.getUserLoggedIn().getEmail());
    }

    /**
     * On create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_workspaces, menu);
        return true;
    }

    /**
     * On options item selected.
     *
     * @param item the item
     * @return true, if successful
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * View foreign workspaces.
     *
     * @param v the v
     */
    public void viewForeignWorkspaces(View v) {
        Intent intent = new Intent(this, ForeignWorkspacesActivity.class);
        startActivity(intent);
    }

    /**
     * Creates the new workspace.
     *
     * @param v the v
     */
    public void createNewWorkspace(View v) {
        // Create an instance of the dialog fragment and show it
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_create_new_workspace);
        dialog.setTitle(R.string.newWorkspaceLabel);
        Button submitButton = (Button) dialog.findViewById(R.id.submitNewWsButton);
        final Activity activity = this;


        final TextView textView = (TextView) dialog.findViewById(R.id.newWsNameText);
        final String ownerEmail = SimpleApp.getUserLoggedIn().getEmail();
        final SeekBar quotaBar = (SeekBar) dialog.findViewById(R.id.newWsQuotaBar);
        final TextView quotaValue = (TextView) dialog.findViewById(R.id.newWsQuotaValueText);


        // Initialize the textview with '0'.
        quotaValue.setText(1 + " MB");
        QuotaBoundariesOfWorkspaceService service = new QuotaBoundariesOfWorkspaceService(
                getApplicationContext());
        service.run(null);
        quotaBar.setMax((int) service.getResult()[1] - 2);
        quotaBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                quotaValue.setText((progressValue + 1) + " MB");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        // if button is clicked, close the custom dialog
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quota = quotaBar.getProgress() + 1;
                String name = textView.getText().toString();
                NewMyWorkspacesAsyncTask task = new NewMyWorkspacesAsyncTask(
                        getApplicationContext(), dialog, activity);
                task.execute(name, ownerEmail, "" + quota);
            }
        });

        Button cancelButton = (Button) dialog.findViewById(R.id.cancelNewWsButton);
        // if button is clicked, close the custom dialog
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


}
