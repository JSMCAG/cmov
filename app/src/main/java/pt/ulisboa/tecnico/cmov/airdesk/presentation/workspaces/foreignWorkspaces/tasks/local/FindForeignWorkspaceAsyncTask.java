package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.local;

/**
 * Created by Group 7 on 06-Apr-15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.ForeignWorkspace;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.ForeignWorkspaceAdapter;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.ForeignWorkspaceOfflineDialog;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FindMyForeignWorkspaceService;

/**
 * The Class FindForeignWorkspaceAsyncTask.
 * Finds the user's Foreign Workspaces
 */
public class FindForeignWorkspaceAsyncTask extends
        AsyncTask<String, Void, ArrayList<WorkspaceAccess>> {

    /**
     * The service.
     */
    private FindMyForeignWorkspaceService service;

    /**
     * The activity.
     */
    private ActionBarActivity activity;

    private ArrayList<ForeignWorkspace> workspacesList = new ArrayList<>();

    /**
     * Instantiates a new find foreign workspace async task.
     *
     * @param context  the context
     * @param activity the activity
     */
    public FindForeignWorkspaceAsyncTask(Context context, ActionBarActivity activity) {
        super();
        this.service = new FindMyForeignWorkspaceService(context);
        this.activity = activity;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the array list
     */
    @Override
    protected ArrayList<WorkspaceAccess> doInBackground(String... params) {
        String foreignEmail = params[0];

        service.run(foreignEmail);

        return service.getResult();
    }

    /**
     * On post execute.
     *
     * @param workspaceAccesses the workspace accesses
     */
    @Override
    protected void onPostExecute(ArrayList<WorkspaceAccess> workspaceAccesses) {
        super.onPostExecute(workspaceAccesses);
        for (WorkspaceAccess w : workspaceAccesses) {
            ForeignWorkspace wsA = new ForeignWorkspace(w.getWorkspaceName(), w.getWorkspaceOwnerEmail());
            workspacesList.add(wsA);
        }

        if (workspacesList.isEmpty()) {
            return;
        }
        ForeignWorkspaceAdapter adapter = new ForeignWorkspaceAdapter(activity, workspacesList, activity.getResources());
        final ListView wsListView = (ListView) activity.findViewById(R.id.foreignWsList);
        final AdapterView.OnItemClickListener wsListViewItemClickListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                final int position = wsListView.getPositionForView(myView);
                ForeignWorkspace title = (ForeignWorkspace) workspacesList.get(position);
                Bundle args = new Bundle();
                args.putString("title", title.getWorkspaceName());
                args.putString("owner_email", title.getWorkspaceOwner());
                showWorkspaceNoticeDialog(args);
            }
        };
        wsListView.setOnItemClickListener(wsListViewItemClickListener);
        wsListView.setAdapter(adapter);
    }

    /**
     * Show workspace notice dialog.
     *
     * @param args the args
     */
    private void showWorkspaceNoticeDialog(Bundle args) {
        // Create an instance of the dialog fragment and show it
        ForeignWorkspaceOfflineDialog dialog = new ForeignWorkspaceOfflineDialog();
        dialog.setArguments(args);
        dialog.show(activity.getSupportFragmentManager(), "NoticeDialogFragment");
    }

}
