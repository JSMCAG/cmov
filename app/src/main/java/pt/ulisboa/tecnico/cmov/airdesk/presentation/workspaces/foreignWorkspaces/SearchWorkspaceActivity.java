package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces;

/**
 * Created by Group 7 on 26-Mar-15.
 */

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.remote.SearchForeignWorkspaceAsyncTask;
//import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.local.SearchForeignWorkspaceAsyncTask;

/**
 * The Class SearchWorkspaceActivity.
 */
public class SearchWorkspaceActivity extends
        ActionBarActivity {

    /**
     * On create.
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_workspace);

        ArrayList<String> l = new ArrayList<>();
        initializeSearchWorkspacesListView(l);

        ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
        P2PNode[] peerArray = new P2PNode[peerList.size()];
        peerArray = peerList.toArray(peerArray);
        SearchForeignWorkspaceAsyncTask task = new SearchForeignWorkspaceAsyncTask(getApplicationContext(), this);

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, peerArray);
        //SearchForeignWorkspaceAsyncTask task = new SearchForeignWorkspaceAsyncTask(getApplicationContext(),this);
        //task.execute("f.apolinario30@hotmail.com");
    }

    /**
     * Initialize search workspaces list view.
     *
     * @param noteList the note list
     */
    private void initializeSearchWorkspacesListView(ArrayList<String> noteList) {
        //create adapter with layout specified in simple_list_item_1 and with noteList's content
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, noteList);

        //specify list view behaviour
        final ListView listView = (ListView) findViewById(R.id.searchForeignWsList);
        //create interaction when list item is pressed
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                final int position = listView.getPositionForView(myView);
                String title = listView.getItemAtPosition(position).toString();
                Bundle args = new Bundle();
                args.putString("title", title);
                showAskToJoinWorkspaceNoticeDialog(args);
            }
        });
        //set listview content
        listView.setAdapter(adapter);
    }

    /**
     * Show ask to join workspace notice dialog.
     *
     * @param args the args
     */
    private void showAskToJoinWorkspaceNoticeDialog(Bundle args) {
        // Create an instance of the dialog fragment and show it
        AskToJoinWorkspaceDialog dialog = new AskToJoinWorkspaceDialog();
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
    }


    /**
     * On create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_workspace, menu);
        return true;
    }

    /**
     * On options item selected.
     *
     * @param item the item
     * @return true, if successful
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * View foreign workspaces.
     *
     * @param v the v
     */
    public void viewForeignWorkspaces(View v) {
        Intent intent = new Intent(this, ForeignWorkspacesActivity.class);
        startActivity(intent);
    }

}
