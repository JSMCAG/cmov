package pt.ulisboa.tecnico.cmov.airdesk.service.exception;

/**
 * Created by Group 7 on 07-Apr-15.
 */

/**
 * The Class EmptyFieldsException.
 * This Exception is raised when a service is called with the wrong number of arguments.
 */
public class EmptyFieldsException extends
        ServiceException {
}
