package pt.ulisboa.tecnico.cmov.airdesk.service.exception;

/**
 * Created by Group 7 on 07-Apr-15.
 */

/**
 * The Class FileAlreadyExistsException.
 * This Exception is raised when a service is trying to create a file that already exists in the workspace.
 */
public class FileAlreadyExistsException extends
        ServiceException {
}
