package pt.ulisboa.tecnico.cmov.airdesk.service.exception;

/**
 * Created by Group 7 on 08-Apr-15.
 */

/**
 * The Class WorkspaceIsPrivateException.
 * This Exception is raised when a Workspace in question needs to be public, but sadly it's private.
 */
public class WorkspaceIsPrivateException extends
        ServiceException {
}
