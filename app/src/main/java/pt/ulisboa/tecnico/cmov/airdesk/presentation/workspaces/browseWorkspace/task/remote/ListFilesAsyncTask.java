package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.remote;

/**
 * Created by Group 7 on 06-Apr-15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.BrowseForeignWorkspaceDialog;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.SearchFilesInWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.fileCommands.ListWorkspaceFilesCommand;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.RequestCommunicationService;

/**
 * The Class ListFilesAsyncTask.
 * This task fetches all files in the workspace from the FileStorage.
 */
public class ListFilesAsyncTask extends
        AsyncTask<P2PNode, String, ArrayList<String>> {

    /**
     * The service1.
     */
    private SearchFilesInWorkspaceService service1;

    /**
     * The activity.
     */
    private ActionBarActivity activity;

    /**
     * The workspace name.
     */
    private String workspaceName;

    /**
     * The owner email.
     */
    private String ownerEmail;

    /**
     * The quota.
     */
    private int quota;

    /**
     * The context.
     */
    private Context context;

    /**
     * Instantiates a new list files async task.
     *
     * @param context       the context
     * @param workspaceName the workspace name
     * @param quota         the quota
     * @param activity      the activity
     */
    public ListFilesAsyncTask(Context context,
                              int quota,
                              String ownerEmail,
                              String workspaceName,
                              ActionBarActivity activity) {
        this.context = context;
        this.quota = quota;
        this.activity = activity;
        this.workspaceName = workspaceName;
        this.ownerEmail = ownerEmail;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected ArrayList<String> doInBackground(P2PNode... params) {
        for (P2PNode peer : params) {
            if (peer.getOwnerEmail().equals(ownerEmail)) {
                ListWorkspaceFilesCommand command = new ListWorkspaceFilesCommand(workspaceName);

                publishProgress("Sending request to peer");
                RequestCommunicationService service = new RequestCommunicationService(command, peer, context);

                try {
                    service.execute();
                } catch (CommunicationException e) {
                    publishProgress("Failed to list Workspace Files");
                    return null;
                }

                ListWorkspaceFilesCommand responseCommand = (ListWorkspaceFilesCommand) service.getResult();
                publishProgress("successfully retrieved workspace files");
                return responseCommand.getResult();
            }
        }
        return null;
    }

    /**
     * On post execute.
     *
     * @param list list of all files in Workspace
     */
    @Override
    protected void onPostExecute(ArrayList<String> list) {
        if (list == null) {
            return;
        }
        initializeBrowseWorkspaceListView(list);
    }

    /**
     * Initialize browse workspace list view.
     *
     * @param noteList the note list
     */
    private void initializeBrowseWorkspaceListView(ArrayList<String> noteList) {
        //create adapter with layout specified in simple_list_item_1 and with noteList's content
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_list_item_1, noteList);

        //specify list view behaviour
        final ListView listView = (ListView) activity.findViewById(R.id.browseWsListView);
        //create interaction when list item is pressed
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                //FIXME CHANGE THIS FOR REMOTE IMPLEMENTATION
                final int position = listView.getPositionForView(myView);
                String title = listView.getItemAtPosition(position).toString();
                Bundle args = new Bundle();
                args.putString("owner_email", ownerEmail);
                args.putString("workspace_name", workspaceName);

                args.putString("folder_abs_path", "");
                args.putString("title", title);
                args.putInt("quota", quota);
                showBrowseWorkspaceNoticeDialog(args);
            }
        });
        //set listview content
        listView.setAdapter(adapter);
    }


    /**
     * Show browse workspace notice dialog.
     *
     * @param args the args
     */
    private void showBrowseWorkspaceNoticeDialog(Bundle args) {
        // Create an instance of the dialog fragment and show it
        BrowseForeignWorkspaceDialog dialog = new BrowseForeignWorkspaceDialog();
        dialog.setArguments(args);
        dialog.show(activity.getSupportFragmentManager(), "NoticeDialogFragment");
    }

}
