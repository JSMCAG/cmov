package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchRequestsWorkspaceAccessService;


/**
 * Created by Group 7 on 17-Mar-15.
 */
public class ViewUserWaitingForWorkspaceAccessDialog extends
        DialogFragment {

    /**
     * The workspace name.
     */
    private String WORKSPACE_NAME;

    /**
     * On create dialog.
     *
     * @param savedInstanceState the saved instance state
     * @return the dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("User waiting for workspace access");
        WORKSPACE_NAME = getArguments().getString("workspace");

        ArrayList<String> l = new ArrayList<>();
        FetchRequestsWorkspaceAccessService service = new FetchRequestsWorkspaceAccessService(getActivity().getApplicationContext());
        service.run(WORKSPACE_NAME, SimpleApp.getUserLoggedIn().getEmail());
        ArrayList<WorkspaceAccess> list = service.getResult();
        for (WorkspaceAccess wsA : list) {
            l.add(wsA.getEmailWithWorkspaceAccess());
        }
        final ArrayAdapter<String> lAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, l);

        final ListView modeList = new ListView(getActivity());
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                final int position = modeList.getPositionForView(myView);
                String user = modeList.getItemAtPosition(position).toString();
                Bundle args = new Bundle();
                args.putString("workspace", WORKSPACE_NAME);
                args.putString("person", user);
                grantAccessToWorkspaceDialog(args);
            }
        });

        modeList.setAdapter(lAdapter);
        builder.setView(modeList);

        // Create the AlertDialog object and return it
        return builder.create();
    }

    /**
     * Grant access to workspace dialog.
     *
     * @param args the args
     */
    private void grantAccessToWorkspaceDialog(Bundle args) {
        GrantAccessToWorkspaceDialog dialog = new GrantAccessToWorkspaceDialog();
        dialog.setArguments(args);
        dialog.show(getActivity().getSupportFragmentManager(), "NoticeDialogFragment");
        this.dismiss();
    }

}
