package pt.ulisboa.tecnico.cmov.airdesk.core.Database;

/**
 * Created by Group 7 on 13-Mar-15.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;

/**
 * The Class WorkspaceDataSource.
 * The main porpoise of this class is to function as a handler for manipulating Workspace table.
 */
public class WorkspaceDataSource {

    // Database fields
    /**
     * The database.
     */
    private SQLiteDatabase database;

    /**
     * The db helper.
     */
    private MySQLiteHelper dbHelper;

    /**
     * The all columns.
     */
    private String[] allColumns = {MySQLiteHelper.COLUMN_NAME, MySQLiteHelper.COLUMN_EMAIL,
            MySQLiteHelper.COLUMN_QUOTA, MySQLiteHelper.COLUMN_IS_PRIVATE,
            MySQLiteHelper.COLUMN_DIRECTORY};

    /**
     * Instantiates a new workspace data source.
     *
     * @param context the context
     */
    public WorkspaceDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    /**
     * Open. This method opens a connection to the AirDesk database (required for manipulating the database).
     *
     * @throws SQLException the SQL exception
     */
    public synchronized void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    /**
     * Close. This method closes a connection to the AirDesk database.
     */
    public synchronized void close() {
        dbHelper.close();
    }

    /**
     * Adds the workspace.
     *
     * @param workspace the workspace
     */
    public synchronized void addWorkspace(Workspace workspace) {
        ContentValues values = new ContentValues();
        //Create Workspace tuple
        values.put(MySQLiteHelper.COLUMN_NAME, workspace.getName());
        values.put(MySQLiteHelper.COLUMN_EMAIL, workspace.getOwnerEmail());
        values.put(MySQLiteHelper.COLUMN_QUOTA, workspace.getQuota());
        int isPrivateInt = booleanToInt(workspace.isPrivateWorkspace());
        values.put(MySQLiteHelper.COLUMN_IS_PRIVATE, isPrivateInt);
        values.put(MySQLiteHelper.COLUMN_DIRECTORY, workspace.getDirectory());

        database.insert(MySQLiteHelper.TABLE_MY_WORKSPACE, null, values);
    }

    /**
     * converts boolean to int
     *
     * @param b the b
     * @return the int
     */
    private int booleanToInt(boolean b) {
        if (b) {
            return 1;
        } else
            return 0;
    }

    /**
     * int to boolean.
     *
     * @param i the i
     * @return true, if successful
     */
    private boolean intToboolean(int i) {
        return i != 0;
    }

    /**
     * Delete workspace from database.
     *
     * @param workspace the workspace
     */
    public synchronized void deleteWorkspace(Workspace workspace) {
        String name = workspace.getName();
        String email = workspace.getOwnerEmail();
        database.delete(MySQLiteHelper.TABLE_MY_WORKSPACE, MySQLiteHelper.COLUMN_NAME + " = '"
                + name + "'" + " AND " + MySQLiteHelper.COLUMN_EMAIL + " = '" + email + "'", null);
    }

    /**
     * Gets the workspaces.
     *
     * @return the workspaces
     */
    public synchronized List<Workspace> getWorkspaces() {
        List<Workspace> workspaces = new ArrayList<>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_MY_WORKSPACE, allColumns, null, null,
                null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Workspace ws = cursorToWorkspace(cursor);
            workspaces.add(ws);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return workspaces;
    }

    /**
     * Cursor to workspace.
     * Converts a database tuple (Cursor) to Workspace Object.
     *
     * @param cursor the cursor
     * @return the workspace
     */
    private synchronized Workspace cursorToWorkspace(Cursor cursor) {
        String name = cursor.getString(0);
        String ownerEmail = cursor.getString(1);
        int quota = cursor.getInt(2);
        boolean isPrivateWorkspace = intToboolean(cursor.getInt(3));
        return new Workspace(name, ownerEmail, quota, isPrivateWorkspace);
    }

    /**
     * Find workspace by name and owner.
     *
     * @param wsName     the ws name
     * @param ownerEmail the owner email
     * @return the workspace
     */
    public synchronized Workspace findWorkspaceByNameAndOwner(String wsName, String ownerEmail) {

        Cursor cursor = database.query(MySQLiteHelper.TABLE_MY_WORKSPACE, allColumns,
                MySQLiteHelper.COLUMN_NAME + "=? AND " + MySQLiteHelper.COLUMN_EMAIL + "=?",
                new String[]{wsName, ownerEmail}, null, null, null, null);

        Workspace ws = null;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            ws = cursorToWorkspace(cursor);
        }
        cursor.close();

        return ws;
    }

    /**
     * Find workspace by owner.
     *
     * @param ownerEmail the owner email
     * @return the array list
     */
    public synchronized ArrayList<Workspace> findWorkspaceByOwner(String ownerEmail) {

        Cursor cursor = database.query(MySQLiteHelper.TABLE_MY_WORKSPACE, allColumns,
                MySQLiteHelper.COLUMN_EMAIL + "=?", new String[]{ownerEmail}, null, null, null,
                null);

        ArrayList<Workspace> ws = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ws.add(cursorToWorkspace(cursor));
                cursor.moveToNext();
            }
        }
        cursor.close();

        return ws;
    }

    /**
     * Find public workspace by owner.
     *
     * @param ownerEmail the owner email
     * @return the array list
     */
    public synchronized ArrayList<Workspace> findPublicWorkspaceByOwner(String ownerEmail) {

        Cursor cursor = database.query(MySQLiteHelper.TABLE_MY_WORKSPACE, allColumns,
                MySQLiteHelper.COLUMN_EMAIL + "=?" + " AND " + MySQLiteHelper.COLUMN_IS_PRIVATE + "= 0", new String[]{ownerEmail}, null, null, null,
                null);

        ArrayList<Workspace> ws = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ws.add(cursorToWorkspace(cursor));
                cursor.moveToNext();
            }
        }
        cursor.close();

        return ws;
    }


    /**
     * Change workspace.
     * updates workspace.
     *
     * @param workspace the workspace
     */
    public void changeWorkspace(Workspace workspace) {
        ContentValues values = new ContentValues();
        //Create Workspace tuple
        values.put(MySQLiteHelper.COLUMN_NAME, workspace.getName());
        values.put(MySQLiteHelper.COLUMN_EMAIL, workspace.getOwnerEmail());
        values.put(MySQLiteHelper.COLUMN_QUOTA, workspace.getQuota());
        int isPrivateInt = booleanToInt(workspace.isPrivateWorkspace());
        values.put(MySQLiteHelper.COLUMN_IS_PRIVATE, isPrivateInt);
        values.put(MySQLiteHelper.COLUMN_DIRECTORY, workspace.getDirectory());
        database.update(MySQLiteHelper.TABLE_MY_WORKSPACE, values, MySQLiteHelper.COLUMN_NAME + "="
                + "'" + workspace.getName() + "'" + " AND " + MySQLiteHelper.COLUMN_EMAIL + "="
                + "'" + workspace.getOwnerEmail() + "'", null);
    }

}
