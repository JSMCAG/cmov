package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.local;

/**
 * Created by Group 7 on 06-Apr-15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.BrowseMyWorkspaceDialog;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.SearchFilesInWorkspaceService;

/**
 * The Class ListFilesAsyncTask.
 * This task fetches all files in the workspace from the FileStorage.
 */
public class ListFilesAsyncTask extends
        AsyncTask<String, Void, Void> {

    /**
     * The service1.
     */
    private SearchFilesInWorkspaceService service1;

    /**
     * The activity.
     */
    private ActionBarActivity activity;

    /**
     * The workspace path.
     */
    private String workspacePath;

    /**
     * The quota.
     */
    private int quota;

    /**
     * Instantiates a new list files async task.
     *
     * @param context       the context
     * @param workspacePath the workspace path
     * @param quota         the quota
     * @param activity      the activity
     */
    public ListFilesAsyncTask(Context context,
                              String workspacePath,
                              int quota,
                              ActionBarActivity activity) {
        this.service1 = new SearchFilesInWorkspaceService(context, workspacePath);
        this.activity = activity;
        this.workspacePath = workspacePath;
        this.quota = quota;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(String... params) {
        //FIXME email is done only for my_workspaces
        service1.run();
        return null;
    }

    /**
     * On post execute.
     *
     * @param aVoid the a void
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        ArrayList<String> l = service1.getResult();
        initializeBrowseWorkspaceListView(l);
    }

    /**
     * Initialize browse workspace list view.
     *
     * @param noteList the note list
     */
    private void initializeBrowseWorkspaceListView(ArrayList<String> noteList) {
        //create adapter with layout specified in simple_list_item_1 and with noteList's content
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_list_item_1, noteList);

        //specify list view behaviour
        final ListView listView = (ListView) activity.findViewById(R.id.browseWsListView);
        //create interaction when list item is pressed
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                final int position = listView.getPositionForView(myView);
                String title = listView.getItemAtPosition(position).toString();
                Bundle args = new Bundle();
                args.putString("folder_abs_path", workspacePath);
                args.putString("title", title);
                args.putInt("quota", quota);
                showBrowseWorkspaceNoticeDialog(args);
            }
        });
        //set listview content
        listView.setAdapter(adapter);
    }


    /**
     * Show browse workspace notice dialog.
     *
     * @param args the args
     */
    private void showBrowseWorkspaceNoticeDialog(Bundle args) {
        // Create an instance of the dialog fragment and show it
        BrowseMyWorkspaceDialog dialog = new BrowseMyWorkspaceDialog();
        dialog.setArguments(args);
        dialog.show(activity.getSupportFragmentManager(), "NoticeDialogFragment");
    }

}
