package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.remote;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.FileDto;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceInstallObject;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ServiceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.CreateFileService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.WriteFileService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.AddPersonToWorkspaceAccessService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.DestroyWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.LeaveWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.NewWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands.DownloadWorkspaceCommand;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.RequestCommunicationService;

/**
 * Created by Filipe Apolinário on 15-May-15.
 */
public class DownloadWorkspaceAsyncTask extends
        AsyncTask<P2PNode, String, Void> {

    /**
     * The service.
     */
    private LeaveWorkspaceService service;

    /**
     * The message.
     */
    private String message;

    /**
     * The context.
     */
    private Context context;

    /**
     * The workspaceName.
     */
    private String workspaceName;

    /**
     * The owner email.
     */
    private String ownerEmail;

    /**
     * The foreign email.
     */
    private String foreignEmail;

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * Instantiates a new leave workspace async task.
     *
     * @param activity      the activity
     * @param context       the context
     * @param workspaceName the workspaceName
     * @param ownerEmail    the owner email
     * @param foreignEmail  the foreign email
     */
    public DownloadWorkspaceAsyncTask(Activity activity,
                                      Context context,
                                      String workspaceName,
                                      String ownerEmail,
                                      String foreignEmail) {
        super();
        //service = new LeaveWorkspaceService(context);
        this.activity = activity;
        this.context = context;
        this.workspaceName = workspaceName;
        this.ownerEmail = ownerEmail;
        this.foreignEmail = foreignEmail;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(P2PNode... params) {
        for (P2PNode peer : params) {
            if (peer.getOwnerEmail().equals(ownerEmail)) {
                try {
                    DownloadWorkspaceCommand command = new DownloadWorkspaceCommand(workspaceName);

                    //send request to peer
                    publishProgress("Sending request to peer");
                    RequestCommunicationService service = new RequestCommunicationService(command, peer, context);
                    service.execute();

                    DownloadWorkspaceCommand responseCommand = (DownloadWorkspaceCommand) service.getResult();
                    WorkspaceInstallObject workspaceInstallObject = responseCommand.getResult();

                    publishProgress("Succefully downloaded Workspace");


                    publishProgress("Starting to install Workspace");
                    try {
                        DestroyWorkspaceService destroyWorkspaceService = new DestroyWorkspaceService(context);
                        destroyWorkspaceService.run(workspaceInstallObject.getWs().getName(), workspaceInstallObject.getWs().getOwnerEmail());
                    } catch (ServiceException e) {
                        //publishProgress("Failed to Install Workspace");

                    }

                    try {
                        NewWorkspaceService newWorkspaceService = new NewWorkspaceService(context);
                        newWorkspaceService.run(workspaceInstallObject.getWs().getName(), workspaceInstallObject.getWs().getOwnerEmail(), workspaceInstallObject.getWs().getQuota(), workspaceInstallObject.getWs().isPrivateWorkspace());

                        for (WorkspaceAccess access : workspaceInstallObject.getL()) {
                            AddPersonToWorkspaceAccessService addPersonToWorkspaceAccessService = new AddPersonToWorkspaceAccessService(context);
                            addPersonToWorkspaceAccessService.run(workspaceInstallObject.getWs().getName(), workspaceInstallObject.getWs().getOwnerEmail(), access.getEmailWithWorkspaceAccess());
                        }

                        for (FileDto fileDto : workspaceInstallObject.getFilesWs()) {
                            CreateFileService createFileService = new CreateFileService(context);
                            createFileService.run(workspaceInstallObject.getWs(), fileDto.getFileName());

                            WriteFileService writeFileService = new WriteFileService(context);
                            writeFileService.run(workspaceInstallObject.getWs().getDirectory(), fileDto.getFileName(), fileDto.getContent(), workspaceInstallObject.getWs().getQuota());
                        }
                        publishProgress("Sucessfully installed Workspace");

                    } catch (ServiceException e) {
                        publishProgress("Failed to Install Workspace");
                    }


                    //TODO install Workspace
                } catch (CommunicationException e) {
                    publishProgress("Could not perform request");
                }
            }
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, values[0], duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    /**
     * On post execute.
     *
     * @param aVoid the a void
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        reloadActivity(activity);
    }

    /**
     * Reload activity.
     *
     * @param activity the activity
     */
    private void reloadActivity(Activity activity) {
        activity.finish();
        activity.startActivity(activity.getIntent());
    }
}
