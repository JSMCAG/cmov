package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceAccessService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 13-May-15.
 */
public class ViewPeopleCommand extends Command {
    private String workspaceName;

    private ArrayList<WorkspaceAccess> result;

    public ViewPeopleCommand(String workspaceName) {
        this.workspaceName = workspaceName;
    }

    public void execute() {

        FetchWorkspaceAccessService service = new FetchWorkspaceAccessService(SimpleApp.getContext());
        service.run(workspaceName, SimpleApp.getUserLoggedIn().getEmail());

        result = service.getResult();
    }

    public ArrayList<WorkspaceAccess> getResult() {
        return result;
    }
}
