package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

/**
 * Created by Group 7 on 04-Apr-15.
 */

import android.content.Context;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;

/**
 * The Class FetchWorkspaceService.
 */
public class FetchWorkspaceService {

    /**
     * The database my workspace handler.
     */
    private WorkspaceDataSource databaseMyWorkspaceHandler;

    /**
     * The ws.
     */
    private Workspace ws;

    /**
     * Instantiates a new fetch workspace service.
     *
     * @param context the context
     */
    public FetchWorkspaceService(Context context) {
        databaseMyWorkspaceHandler = new WorkspaceDataSource(context);
    }

    /**
     * Run.
     *
     * @param ownerEmail the owner email
     * @param wsName     the ws name
     */
    public void run(String ownerEmail, String wsName) {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {wsName, ownerEmail};
        if (!verifier.stringVerifier(sParams)) {
            return;
        }
        databaseMyWorkspaceHandler.open();
        this.ws = databaseMyWorkspaceHandler.findWorkspaceByNameAndOwner(wsName, ownerEmail);
        databaseMyWorkspaceHandler.close();
    }

    /**
     * Gets the result.
     *
     * @return the result
     */
    public Workspace getResult() {
        return ws;
    }
}
