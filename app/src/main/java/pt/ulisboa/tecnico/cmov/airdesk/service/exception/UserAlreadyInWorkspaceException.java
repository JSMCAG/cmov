package pt.ulisboa.tecnico.cmov.airdesk.service.exception;

/**
 * Created by Group 7 on 08-Apr-15.
 */


/**
 * The Class UserAlreadyInWorkspaceException.
 * This exception is raised when the service is trying to add to a Workspace an User that already belong to the Workspace.
 */
public class UserAlreadyInWorkspaceException extends
        ServiceException {
}
