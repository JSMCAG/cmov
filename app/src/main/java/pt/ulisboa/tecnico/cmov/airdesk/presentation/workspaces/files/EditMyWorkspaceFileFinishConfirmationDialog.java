package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files;

/**
 * Created by Group 7 on 14-Mar-15.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files.task.local.EditFileAsyncTask;

/**
 * The Class EditFileFinishConfirmationDialog.
 */
public class EditMyWorkspaceFileFinishConfirmationDialog extends
        DialogFragment {

    /**
     * The file name.
     */
    private String FILE_NAME;

    /**
     * The folder abs path.
     */
    private String FOLDER_ABS_PATH;

    /**
     * The text.
     */
    private String TEXT;

    /**
     * The quota.
     */
    private int QUOTA;

    /**
     * On create dialog.
     *
     * @param savedInstanceState the saved instance state
     * @return the dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        Bundle args = this.getArguments();

        FILE_NAME = args.getString("file_name");
        FOLDER_ABS_PATH = args.getString("folder_abs_path");
        TEXT = args.getString("body");
        QUOTA = args.getInt("quota");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Save and Exit");

        builder.setMessage("Do you want save and exit the file editor?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Activity activity = getActivity();
                        EditFileAsyncTask task = new EditFileAsyncTask(activity
                                .getApplicationContext(), getActivity(), FOLDER_ABS_PATH,
                                FILE_NAME, TEXT, QUOTA);
                        task.execute();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Intentionally left black, user cancelled the dialog
            }
        });

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
