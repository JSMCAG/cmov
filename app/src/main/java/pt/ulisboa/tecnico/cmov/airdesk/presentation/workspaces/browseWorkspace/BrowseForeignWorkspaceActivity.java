package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace;

/**
 * Created by Group 7 on 26-Mar-15.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.remote.CreateFileAsyncTask;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.remote.FetchWorkspaceAsyncTask;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.remote.FetchWorkspaceInformationAsyncTask;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.remote.ViewPeopleAsyncTask;

/**
 * The Class BrowseWorkspaceActivity.
 * This activity allows to do operations on the workspace (WORKSPACE_NAME)
 * About Workspace:  Shows the details of the Workspace
 * View People: List Users with Workspace access.
 * File Operations:
 * Create File
 * Read File
 * Edit File
 * Delete File
 */
public class BrowseForeignWorkspaceActivity extends
        ActionBarActivity {

    /**
     * The ws.
     */
    public Workspace ws;
    /**
     * The workspace name.
     */
    private String WORKSPACE_NAME;
    private String OWNER_EMAIL;

    /**
     * On save instance state.
     *
     * @param outState the out state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("workspace", WORKSPACE_NAME);
        outState.putString("owner_email", OWNER_EMAIL);
    }

    /**
     * On create.
     * this method initializes components, including the ListView with all files in the workspace
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_workspace);

        Intent i = getIntent();
        WORKSPACE_NAME = i.getStringExtra("workspace");
        OWNER_EMAIL = i.getStringExtra("owner_email");
        initializeBrowseWorkspaceActivityTitle(WORKSPACE_NAME);

        ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
        P2PNode[] peerArray = new P2PNode[peerList.size()];
        peerArray = peerList.toArray(peerArray);
        FetchWorkspaceAsyncTask task = new FetchWorkspaceAsyncTask(this, getApplicationContext(), WORKSPACE_NAME, OWNER_EMAIL);
        task.execute(peerArray);
    }

    /**
     * Initialize browse workspace activity title.
     *
     * @param WORKSPACE_NAME the workspace name
     */
    private void initializeBrowseWorkspaceActivityTitle(String WORKSPACE_NAME) {
        TextView t = (TextView) findViewById(R.id.browseWsLabel);
        t.setText(WORKSPACE_NAME);
    }

    /**
     * Shows users in the workspace
     *
     * @param v the v
     */
    public void showUsersNoticeDialog(View v) {
        ViewPeopleAsyncTask task = new ViewPeopleAsyncTask(getApplicationContext(), this, WORKSPACE_NAME, OWNER_EMAIL);


        ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
        P2PNode[] peerArray = new P2PNode[peerList.size()];
        peerArray = peerList.toArray(peerArray);
        task.execute(peerArray);
    }

    /**
     * On create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_browse_workspace, menu);
        return true;
    }

    /**
     * On options item selected.
     *
     * @param item the item
     * @return true, if successful
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates the file.
     *
     * @param v the v
     */
    public void createFile(View v) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_create_new_file);
        dialog.setTitle("Create New File:");
        Button submitButton = (Button) dialog.findViewById(R.id.submitNewWsButton);
        final Activity activity = this;
        // if button is clicked, close the custom dialog
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) dialog.findViewById(R.id.newFileNameText);
                String fileName = textView.getText().toString();

                ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
                P2PNode[] peerArray = new P2PNode[peerList.size()];
                peerArray = peerList.toArray(peerArray);

                CreateFileAsyncTask task = new CreateFileAsyncTask(getApplicationContext(), dialog,
                        activity, OWNER_EMAIL, WORKSPACE_NAME, fileName);
                task.execute(peerArray);
            }
        });

        Button cancelButton = (Button) dialog.findViewById(R.id.cancelNewWsButton);
        // if button is clicked, close the custom dialog
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * About workspace.
     *
     * @param v the v
     */
    public void aboutWorkspace(View v) {
        FetchWorkspaceInformationAsyncTask task = new FetchWorkspaceInformationAsyncTask(this, getApplicationContext(),
                WORKSPACE_NAME, OWNER_EMAIL);
        ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
        P2PNode[] peerArray = new P2PNode[peerList.size()];
        peerArray = peerList.toArray(peerArray);
        task.execute(peerArray);
    }

}
