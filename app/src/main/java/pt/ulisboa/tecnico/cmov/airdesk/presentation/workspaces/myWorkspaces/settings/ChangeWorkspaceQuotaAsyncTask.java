package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.settings;

/**
 * Created by Group 7 on 22-Mar-15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.QuotaExceededException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.ChangeWorkspaceQuotaService;

/**
 * The Class ChangeWorkspaceQuotaAsyncTask.
 * This task updates Workspace's quota.
 */
public class ChangeWorkspaceQuotaAsyncTask extends
        AsyncTask<String, Void, Void> {

    /**
     * The service.
     */
    private ChangeWorkspaceQuotaService service;

    /**
     * The message.
     */
    private String message;

    /**
     * The context.
     */
    private Context context;

    /**
     * Instantiates a new change workspace quota async task.
     *
     * @param context the context
     */
    public ChangeWorkspaceQuotaAsyncTask(Context context) {
        super();
        service = new ChangeWorkspaceQuotaService(context);
        this.context = context;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(String... params) {
        String name = params[0];
        String ownerEmail = params[1];
        int newQuota = Integer.parseInt(params[2]);
        try {
            service.run(name, ownerEmail, newQuota);
            this.message = "Successfully changed quota.";
        } catch (EmptyFieldsException e) {
            this.message = "Some fields were empty.";
        } catch (QuotaExceededException e) {
            this.message = "Quota is wrong.";
        }

        return null;
    }

    /**
     * On post execute.
     *
     * @param aVoid the a void
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }
}
