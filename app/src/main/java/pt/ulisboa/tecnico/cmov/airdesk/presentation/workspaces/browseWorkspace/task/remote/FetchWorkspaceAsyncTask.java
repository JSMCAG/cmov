package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.remote;

import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.BrowseForeignWorkspaceActivity;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands.FetchWorkspaceCommand;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.RequestCommunicationService;

/**
 * Created by Filipe Apolinário on 12-May-15.
 */
public class FetchWorkspaceAsyncTask extends AsyncTask<P2PNode, String, Workspace> {
    BrowseForeignWorkspaceActivity activity;
    String wsName;
    String ownerEmail;
    Context context;

    public FetchWorkspaceAsyncTask(BrowseForeignWorkspaceActivity activity,
                                   Context context,
                                   String wsName,
                                   String ownerEmail) {
        this.context = context;
        this.wsName = wsName;
        this.activity = activity;
        this.ownerEmail = ownerEmail;
    }

    @Override
    protected Workspace doInBackground(P2PNode... params) {
        for (P2PNode peer : params) {
            if (peer.getOwnerEmail().equals(ownerEmail)) {

                try {
                    FetchWorkspaceCommand command = new FetchWorkspaceCommand(wsName);

                    publishProgress("Sending request to peer");
                    RequestCommunicationService service = new RequestCommunicationService(command, peer, context);
                    service.execute();
                    FetchWorkspaceCommand responseCommand = (FetchWorkspaceCommand) service.getResult();
                    publishProgress("succesfully retrived workspace");
                    return responseCommand.getResult();
                } catch (CommunicationException e) {
                    publishProgress("Failed to retrieve Workspace");
                    return null;
                }
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, values[0], duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    @Override
    protected void onPostExecute(Workspace workspace) {
        super.onPostExecute(workspace);

        if (workspace != null) {
            activity.ws = workspace;
            ListFilesAsyncTask task = new ListFilesAsyncTask(context,
                    workspace.getQuota(), ownerEmail, wsName, activity);

            ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
            P2PNode[] peerArray = new P2PNode[peerList.size()];
            peerArray = peerList.toArray(peerArray);

            task.execute(peerArray);
        } else activity.finish();
    }
}
