package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.ForeignWorkspace;

/**
 * Created by Filipe Apolinário on 01-May-15.
 */
public class ForeignWorkspaceAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    public Resources res;
    ForeignWorkspace tempValues = null;
    int i = 0;
    /**
     * ******** Declare Used Variables ********
     */
    private Activity activity;
    private ArrayList data;

    /**
     * **********  CustomAdapter Constructor ****************
     */
    public ForeignWorkspaceAdapter(Activity a, ArrayList d, Resources resLocal) {

        /********** Take passed values **********/
        activity = a;
        data = d;
        res = resLocal;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = (LayoutInflater) activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /**
     * ***** What is the size of Passed Arraylist Size ***********
     */
    public int getCount() {

        if (data.size() <= 0)
            return 1;
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /**
     * *** Depends upon data size called for each row , Create each ListView row ****
     */
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.tabitem, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.text = (TextView) vi.findViewById(R.id.text);
            holder.text1 = (TextView) vi.findViewById(R.id.text1);

            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (data.size() <= 0) {
            holder.text.setText("No Data");

        } else {
            /***** Get each Model object from Arraylist ********/
            tempValues = null;
            tempValues = (ForeignWorkspace) data.get(position);

            /************  Set Model values in Holder elements ***********/

            holder.text.setText(tempValues.getWorkspaceName());
            holder.text1.setText(tempValues.getWorkspaceOwner());
        }
        return vi;
    }

    /**
     * ****** Create a holder Class to contain inflated xml file elements ********
     */
    public static class ViewHolder {

        public TextView text;
        public TextView text1;

    }


}
