package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.fileCommands;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ErrorOccurredException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.FileAlreadyExistsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ServiceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.WriteFileService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 10-May-15.
 */
public class EditFileCommand extends Command {
    private String workspaceName;
    private String fileName;
    private String text;

    private String result;

    public EditFileCommand(String workspaceName, String fileName, String text) {
        this.workspaceName = workspaceName;
        this.fileName = fileName;
        this.text = text;
    }

    public void execute() {
        try {
            FetchWorkspaceService workspaceService = new FetchWorkspaceService(SimpleApp.getContext());
            workspaceService.run(SimpleApp.getUserLoggedIn().getEmail(), workspaceName);

            Workspace ws = workspaceService.getResult();

            WriteFileService service = new WriteFileService(SimpleApp.getContext());
            service.run(ws.getDirectory(), fileName, text, ws.getQuota());
            result = "Successfully created file.";
        } catch (EmptyFieldsException e) {
            result = "Some fields were empty.";
        } catch (FileAlreadyExistsException e) {
            result = "File already exists.";
        } catch (ErrorOccurredException e) {
            result = "An error occurred.";
        } catch (ServiceException e) {
            result = "An error occurred.";
        }
    }

    public String getResult() {
        return result;
    }
}
