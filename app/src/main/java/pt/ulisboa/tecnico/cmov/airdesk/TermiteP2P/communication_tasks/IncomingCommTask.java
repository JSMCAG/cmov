package pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.communication_tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocketServer;
import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;

/**
 * Created by Filipe Apolinário on 09-May-15.
 */
/*
     * Classes implementing chat message exchange
	 */

public class IncomingCommTask extends AsyncTask<Void, SimWifiP2pSocket, Void> {
    public static final String TAG = "AirDesk";
    private Context context;

    public IncomingCommTask(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {

        Log.d(TAG, "IncommingCommTask started (" + this.hashCode() + ").");
        SimWifiP2pSocketServer commandSocketServer;

        //Initialize socket to listen to peers commands
        try {
            commandSocketServer = new SimWifiP2pSocketServer(
                    Integer.parseInt(context.getString(R.string.port)));
            SimpleApp.getWifiDirectStateKeeper().setmSrvSocket(commandSocketServer);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        //Listen to peer connection requests
        while (!Thread.currentThread().isInterrupted()) {
            try {
                SimWifiP2pSocket sock = commandSocketServer.accept();
                //handle connection
                publishProgress(sock);
            } catch (IOException e) {
                Log.d("Error accepting socket:", e.getMessage());
                break;
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(SimWifiP2pSocket... values) {
        //Launch new task to handle peer connection request
        CommandReceiverTask task = new CommandReceiverTask(context);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, values[0]);
    }
}
