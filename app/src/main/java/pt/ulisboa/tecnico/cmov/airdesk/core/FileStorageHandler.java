package pt.ulisboa.tecnico.cmov.airdesk.core;

/**
 * Created by Group 7 on 06-Apr-15.
 */

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ErrorOccurredException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.FileAlreadyExistsException;

/**
 * The Class FileStorageHandler.
 * The main porpoise of this class is to function as a handler for manipulating File Storage.
 */
public class FileStorageHandler {

    /**
     * Creates the directory.
     *
     * @param absolutePath the absolute path
     */
    public void createDirectory(String absolutePath) {
        String folderPath = "";
        String[] dirs = absolutePath.split("/");
        for (String nestedFolder : dirs) {
            folderPath += File.separator + nestedFolder;
            File folder = new File(folderPath);
            if (!folder.exists()) {
                if (folder.mkdir()) {
                    Log.d("Create File", "successfully created folder " + nestedFolder);
                }
            }
        }
    }

    /**
     * Gets the name of the files in workspace.
     *
     * @param workspaceAbsolutePath the workspace absolute path
     * @return the array list
     */
    public ArrayList<String> getNameOfFilesInWorkspace(String workspaceAbsolutePath) {
        File folder = new File(workspaceAbsolutePath);
        return new ArrayList<>(Arrays.asList(folder.list()));
    }

    /**
     * Does directory exist.
     *
     * @param workspaceAbsolutePath the workspace absolute path
     * @return true, if successful
     */
    public boolean doesDirectoryExist(String workspaceAbsolutePath) {
        File folder = new File(workspaceAbsolutePath);
        return folder.exists();
    }

    /**
     * Creates the new file.
     *
     * @param dirPath  the dir path
     * @param fileName the file name
     */
    public void createNewFile(String dirPath, String fileName) {
        File file = new File(dirPath, fileName);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                throw new ErrorOccurredException();
            }
        } else {
            throw new FileAlreadyExistsException();
        }
    }

    /**
     * Write.
     *
     * @param fileAbsPath the file abs path
     * @param body        the body
     */
    public void write(String fileAbsPath, String body) {
        FileOutputStream fos = null;
        try {
            // note that there are many modes you can use
            fos = new FileOutputStream(new File(fileAbsPath));
            fos.write(body.getBytes());
        } catch (FileNotFoundException e) {
            Log.e("Write File", "File not found", e);
        } catch (IOException e) {
            Log.e("Write File", "IO problem", e);
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                Log.d("FileExplorer", "Close error.");
            }
        }
    }

    /**
     * Read.
     *
     * @param fileAbsPAth the file abs p ath
     * @return the string
     */
    public String read(String fileAbsPAth) {
        FileInputStream fis = null;
        Scanner scanner = null;
        StringBuilder sb = new StringBuilder();
        try {
            fis = new FileInputStream(new File(fileAbsPAth));
            // scanner does mean one more object, but it's easier to work with
            scanner = new Scanner(fis);
            while (scanner.hasNextLine()) {
                sb.append(scanner.nextLine() + "\n");
            }
            //Toast.makeText(this, "File read", Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            Log.e("Read File", "File not found", e);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    Log.d("FileExplorer", "Close error.");
                }
            }
            if (scanner != null) {
                scanner.close();
            }
        }
        return sb.toString();
    }

    /**
     * Delete file.
     *
     * @param fileAbsPAth the file abs p ath
     */
    public void deleteFile(String fileAbsPAth) {
        File file = new File(fileAbsPAth);
        if (file.exists()) {
            file.delete();
        }
    }

    /**
     * Checks if is quota valid.
     *
     * @param quota     the quota
     * @param wsAbsPath the ws abs path
     * @return true, if is quota in limit
     */
    public boolean isQuotaValid(int quota, String wsAbsPath) {
        long occupiedSpace = byteToMegaByte(getUsedSpaceAmount(wsAbsPath));
        long freeSpace = byteToMegaByte(getFreeSpaceAmount(wsAbsPath));

        return quota > occupiedSpace && quota < (occupiedSpace + freeSpace);
    }

    /**
     * Gets the amount of Free space.
     *
     * @param wsAbsPath the ws abs path
     * @return the long
     */
    public long getFreeSpaceAmount(String wsAbsPath) {
        File folder = new File(wsAbsPath);

        //Convert to KBytes
        return folder.getFreeSpace();
    }

    /**
     * Gets the used space amount.
     *
     * @param absolutePath the absolute path
     * @return the long
     */
    public long getUsedSpaceAmount(String absolutePath) {
        File folder = new File(absolutePath);
        long occupiedSpace = 0;
        for (File f : folder.listFiles()) {
            occupiedSpace += f.length();
        }

        return occupiedSpace;
    }

    /**
     * Can write in workspace.
     *
     * @param textSize     the text kb size
     * @param quota        the quota
     * @param absolutePath the absolute path
     * @return true, if successful
     */
    public boolean canWriteInWorkspace(int textSize, int quota, String absolutePath) {
        return (byteToMegaByte(getUsedSpaceAmount(absolutePath) + textSize)) <= quota;
    }

    /**
     * Removes the directory.
     *
     * @param absolutePath the absolute path
     */
    public void removeDirectory(String absolutePath) {
        File folder = new File(absolutePath);
        for (File f : folder.listFiles()) {
            f.delete();
        }
        folder.delete();
    }

    /**
     * Byte to mega byte.
     *
     * @param b the b
     * @return the long
     */
    public long byteToMegaByte(long b) {
        long result = b / (1024 * 1024);
        int remainder = (b % (1024 * 1024) > 0) ? 1 : 0;
        return result + remainder;
    }
}
