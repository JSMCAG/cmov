package pt.ulisboa.tecnico.cmov.airdesk.service.exception;

/**
 * Created by Group 7 on 08-Apr-15.
 */

/**
 * The Class ErrorOccurredException.
 * This Exception is raised when a service attempts to do something he is not supposed to do.
 */
public class ErrorOccurredException extends
        ServiceException {
}
