package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces;

/**
 * Created by Group 7 on 08-Apr-15.
 */

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceDoesNotExistException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.DestroyWorkspaceService;

/**
 * The Class DestroyWorkspaceAsyncTask.
 * This task destroys the Workspaces, removing it from the Database and FileStorage.
 */
public class DestroyWorkspaceAsyncTask extends
        AsyncTask<String, Void, Void> {

    /**
     * The service.
     */
    private DestroyWorkspaceService service;

    /**
     * The message.
     */
    private String message;

    /**
     * The context.
     */
    private Context context;

    /**
     * The name.
     */
    private String name;

    /**
     * The owner email.
     */
    private String ownerEmail;

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * Instantiates a new destroy workspace async task.
     *
     * @param activity   the activity
     * @param context    the context
     * @param name       the name
     * @param ownerEmail the owner email
     */
    public DestroyWorkspaceAsyncTask(Activity activity,
                                     Context context,
                                     String name,
                                     String ownerEmail) {
        super();
        this.service = new DestroyWorkspaceService(context);
        this.context = context;
        this.name = name;
        this.ownerEmail = ownerEmail;
        this.activity = activity;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(String... params) {

        try {
            service.run(name, ownerEmail);
            this.message = "Successfully destroyed Workspace.";
        } catch (EmptyFieldsException e) {
            this.message = "Some fields were empty.";
        } catch (WorkspaceDoesNotExistException e) {
            this.message = "Workspace does not exist.";
        }

        return null;
    }

    /**
     * On post execute.
     *
     * @param aVoid the a void
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();

        reloadActivity(activity);
    }

    /**
     * Reload activity.
     *
     * @param activity the activity
     */
    private void reloadActivity(Activity activity) {
        activity.finish();
        activity.startActivity(activity.getIntent());
    }

}
