package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces;


/**
 * Created by Group 7 on 14-Mar-15.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.remote.AskToJoinWorkspaceAsyncTask;
//import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.local.AskToJoinWorkspaceAsyncTask;

/**
 * The Class AskToJoinWorkspaceDialog.
 */
public class AskToJoinWorkspaceDialog extends
        DialogFragment {

    /**
     * On create dialog.
     *
     * @param savedInstanceState the saved instance state
     * @return the dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String WORKSPACE_NAME = getArguments().getString("title");
        final String OWNER_EMAIL = getArguments().getString("owner_email");
        builder.setTitle("Join Workspace " + "'" + WORKSPACE_NAME + "'");

        builder.setMessage("Do you want to join workspace " + "\'" + WORKSPACE_NAME + "\'" + "?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //                      AskToJoinWorkspaceAsyncTask task = new AskToJoinWorkspaceAsyncTask(getActivity().getApplicationContext(),getActivity());
                        //                      task.execute(WORKSPACE_NAME, OWNER_EMAIL, SimpleApp.getUserLoggedIn().getEmail());
                        AskToJoinWorkspaceAsyncTask task = new AskToJoinWorkspaceAsyncTask(getActivity().getApplicationContext(), getActivity(), OWNER_EMAIL, WORKSPACE_NAME, SimpleApp.getUserLoggedIn().getEmail());
                        ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
                        P2PNode[] peerArray = new P2PNode[peerList.size()];
                        peerArray = peerList.toArray(peerArray);

                        task.execute(peerArray);
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Intentionally left black, user cancelled the dialog
            }
        });

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
