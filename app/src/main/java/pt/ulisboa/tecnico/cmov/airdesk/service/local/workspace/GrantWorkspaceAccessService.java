package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

import android.content.Context;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.GrantWorkspaceAccessDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceAccessDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserHasNotRequestedWorkspaceAccessException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceDoesNotExistException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceIsPrivateException;

/**
 * Created by Filipe Apolinário on 18-Apr-15.
 */
public class GrantWorkspaceAccessService {
    /**
     * The database people with workspace access handler.
     */
    private WorkspaceAccessDataSource databasePeopleWithWorkspaceAccessHandler;

    /**
     * The database people waiting for workspace access handler.
     */
    private GrantWorkspaceAccessDataSource databaseRequestWorkspaceAccessHandler;

    /**
     * The context.
     */
    private Context context;

    /**
     * Instantiates a new adds the person to workspace access service.
     *
     * @param context the context
     */
    public GrantWorkspaceAccessService(Context context) {
        this.context = context;
        databaseRequestWorkspaceAccessHandler = new GrantWorkspaceAccessDataSource(context);
        databasePeopleWithWorkspaceAccessHandler = new WorkspaceAccessDataSource(context);
    }

    /**
     * Run.
     *
     * @param wsName       the ws name
     * @param wsOwnerEmail the ws owner email
     * @param foreignEmail the foreign email
     */
    public void run(String wsName, String wsOwnerEmail, String foreignEmail) {
        FieldVerifier verifier = new FieldVerifier();
        String[] params = {wsName, wsOwnerEmail, foreignEmail};
        if (!verifier.stringVerifier(params)) {
            throw new EmptyFieldsException();
        }
        //Verify workspace exists and if it is a public Workspace.
        FetchWorkspaceService service = new FetchWorkspaceService(context);
        service.run(wsOwnerEmail, wsName);
        Workspace ws = service.getResult();
        if (ws == null) {
            throw new WorkspaceDoesNotExistException();
        }
        if (ws.isPrivateWorkspace()) {
            throw new WorkspaceIsPrivateException();
        }
        databaseRequestWorkspaceAccessHandler.open();
        if (!databaseRequestWorkspaceAccessHandler.isForeignInWorkspaceRequest(wsName, wsOwnerEmail, foreignEmail)) {
            databaseRequestWorkspaceAccessHandler.close();
            throw new UserHasNotRequestedWorkspaceAccessException();
        }
        //Ok lets remove user from the request workspace access table!
        WorkspaceAccess wsA = new WorkspaceAccess(wsName, wsOwnerEmail, foreignEmail);
        databaseRequestWorkspaceAccessHandler.removeWorkspaceRequestAccess(wsA);
        databaseRequestWorkspaceAccessHandler.close();
        databasePeopleWithWorkspaceAccessHandler.open();
        if (!databasePeopleWithWorkspaceAccessHandler.isForeignInWorkspace(wsName, wsOwnerEmail, foreignEmail)) {
            databasePeopleWithWorkspaceAccessHandler.addWorkspaceAccess(wsA);
        }
        databasePeopleWithWorkspaceAccessHandler.close();

    }
}
