package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserAlreadyInWorkspaceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserAlreadyRequestedWorkspaceAccessException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceDoesNotExistException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceIsPrivateException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.RequestWorkspaceAccessService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 11-May-15.
 */
public class AskToJoinWorkspaceCommand extends Command {
    private String result;
    private String wsName;
    private String ownerEmail;
    private String foreignEmail;

    {
        result = null;
        wsName = null;
        ownerEmail = null;
        foreignEmail = null;
    }

    public AskToJoinWorkspaceCommand() {

    }

    public AskToJoinWorkspaceCommand(String wsName, String foreignEmail) {
        this.wsName = wsName;
        this.ownerEmail = null;
        this.foreignEmail = foreignEmail;
    }


    @Override
    public void execute() {
        RequestWorkspaceAccessService service = new RequestWorkspaceAccessService(SimpleApp.getContext());

        try {
            this.ownerEmail = SimpleApp.getUserLoggedIn().getEmail();
            service.run(wsName, ownerEmail, foreignEmail);
            result = "Successfully requested to join workspace" + "\"" + wsName + "\".";
        } catch (EmptyFieldsException e) {
            result = "Some fields were empty.";
        } catch (WorkspaceIsPrivateException e) {
            result = "Can not join a private workspace!";
        } catch (WorkspaceDoesNotExistException e) {
            result = "Error: Workspace does not exist!";
        } catch (UserAlreadyRequestedWorkspaceAccessException e) {
            result = "You already have requested workspace access.";
        } catch (UserAlreadyInWorkspaceException e) {
            result = "You already have workspace access.";
        }/* catch (Exception e){
            result = e.getMessage();
        }*/
    }

    public String getResult() {
        return result;
    }
}
