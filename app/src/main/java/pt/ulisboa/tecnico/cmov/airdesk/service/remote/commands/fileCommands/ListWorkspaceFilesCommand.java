package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.fileCommands;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.SearchFilesInWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 12-May-15.
 */
public class ListWorkspaceFilesCommand extends Command {
    private String wsName;
    private ArrayList<String> result = null;

    public ListWorkspaceFilesCommand(String wsName) {
        this.wsName = wsName;
    }

    public void execute() {
        //Fetch workspace
        FetchWorkspaceService workspaceService = new FetchWorkspaceService(SimpleApp.getContext());
        workspaceService.run(SimpleApp.getUserLoggedIn().getEmail(), wsName);

        Workspace ws = workspaceService.getResult();
        //Fetch the name of files in Workspace
        SearchFilesInWorkspaceService service = new SearchFilesInWorkspaceService(SimpleApp.getContext(), ws.getDirectory());
        service.run();
        result = service.getResult();
    }

    public ArrayList<String> getResult() {
        return result;
    }
}
