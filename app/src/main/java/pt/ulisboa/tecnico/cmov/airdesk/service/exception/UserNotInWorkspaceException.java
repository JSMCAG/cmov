package pt.ulisboa.tecnico.cmov.airdesk.service.exception;

/**
 * Created by Group 7 on 08-Apr-15.
 */


/**
 * The Class UserNotInWorkspaceException.
 * This exception is raised when the service expects that the  given User belongs in the Workspace, but he does not.
 */
public class UserNotInWorkspaceException extends
        ServiceException {
}
