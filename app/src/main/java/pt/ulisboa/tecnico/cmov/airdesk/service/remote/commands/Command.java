package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands;

import java.io.Serializable;

/**
 * Created by Filipe Apolinário on 10-May-15.
 */
public abstract class Command implements Serializable {
    private final long serialVersionUID = 5950169519310163575L;

    public Command() {
    }

    public abstract void execute();
}
