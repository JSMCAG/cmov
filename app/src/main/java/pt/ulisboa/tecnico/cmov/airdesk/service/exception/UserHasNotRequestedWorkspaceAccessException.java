package pt.ulisboa.tecnico.cmov.airdesk.service.exception;

/**
 * Created by Filipe Apolinário on 18-Apr-15.
 */
public class UserHasNotRequestedWorkspaceAccessException extends ServiceException {
}
