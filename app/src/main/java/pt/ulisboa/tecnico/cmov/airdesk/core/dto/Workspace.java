package pt.ulisboa.tecnico.cmov.airdesk.core.dto;

/**
 * Created by Group 7 on 23-Mar-15.
 */

import java.io.File;

/**
 * The Class Workspace.
 * The main porpoise of this class is to function as a Data Transfer Object between layers.
 */
public class Workspace extends DTO {


    /**
     * The name.
     */
    private String name;
    /**
     * The owner email.
     */
    private String ownerEmail;
    /**
     * The quota.
     */
    private int quota;
    /**
     * The is private workspace.
     */
    private boolean isPrivateWorkspace;
    /**
     * The directory.
     */
    private String directory;

    {
        isPrivateWorkspace = true;
    }

    public Workspace() {

    }

    /**
     * Instantiates a new workspace.
     *
     * @param name       the name
     * @param ownerEmail the owner email
     * @param quota      the quota
     */
    public Workspace(String name, String ownerEmail, int quota) {
        this.name = name;
        this.ownerEmail = ownerEmail;
        this.quota = quota;
        String directory = ownerEmail + File.separator + name;
        this.directory = removeInvalidFSSymbolsFromDirectory(directory);
    }

    /**
     * Instantiates a new workspace.
     *
     * @param name               the name
     * @param ownerEmail         the owner email
     * @param quota              the quota
     * @param isPrivateWorkspace the is private workspace
     */
    public Workspace(String name, String ownerEmail, int quota, boolean isPrivateWorkspace) {
        this(name, ownerEmail, quota);
        this.isPrivateWorkspace = isPrivateWorkspace;
    }

    /**
     * Removes the invalid fs symbols from directory.
     *
     * @param ownerEmail the owner email
     * @return the string
     */
    private String removeInvalidFSSymbolsFromDirectory(String ownerEmail) {
        return ownerEmail.replaceAll("[^\\./\\p{L}\\p{Nd}]+", "");
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the owner email.
     *
     * @return the owner email
     */
    public String getOwnerEmail() {
        return ownerEmail;
    }

    /**
     * Gets the quota.
     *
     * @return the quota
     */
    public int getQuota() {
        return quota;
    }

    /**
     * Sets the quota.
     *
     * @param quota the new quota
     */
    public void setQuota(int quota) {
        this.quota = quota;
    }

    /**
     * Checks if is private workspace.
     *
     * @return true, if is private workspace
     */
    public boolean isPrivateWorkspace() {
        return isPrivateWorkspace;
    }

    /**
     * Sets the private workspace.
     *
     * @param isPrivateWorkspace the new private workspace
     */
    public void setPrivateWorkspace(boolean isPrivateWorkspace) {
        this.isPrivateWorkspace = isPrivateWorkspace;
    }

    /**
     * Gets the visibility.
     *
     * @return the visibility
     */
    public String getVisibility() {
        if (isPrivateWorkspace) {
            return "Private";
        } else
            return "Public";
    }

    /**
     * Gets the directory.
     *
     * @return the directory
     */
    public String getDirectory() {
        return directory;
    }

    /**
     * Sets the directory.
     *
     * @param directory the new directory
     */
    public void setDirectory(String directory) {
        this.directory = directory;
    }
}
