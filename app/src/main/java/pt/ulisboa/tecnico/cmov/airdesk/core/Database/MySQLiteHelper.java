package pt.ulisboa.tecnico.cmov.airdesk.core.Database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * The Class MySQLiteHelper.
 *
 * @author Group 7
 * @since 13-Mar-15.
 * This class is responsible for creating Database AirDesk and initializing the Tables: USER, WORKSPACE and WORKSPACE_ACCESS.
 */
public class MySQLiteHelper extends
        SQLiteOpenHelper {

    //-TABLE USER DETAILS:

    /**
     * The Constant TABLE_USER.
     * Stores users. Users are identified by email.
     */
    public static final String TABLE_USER = "user";
    /**
     * The Constant COLUMN_EMAIL.
     */
    public static final String COLUMN_EMAIL = "email";
    // table creation sql statement
    /**
     * The Constant DATABASE_CREATE_USER_TABLE.
     */
    private static final String DATABASE_CREATE_USER_TABLE = "create table " + TABLE_USER + "("
            + COLUMN_EMAIL + " TEXT primary key);";

    //-TABLE WORKSPACE DETAILS:
    /**
     * The Constant COLUMN_OWNER_EMAIL.
     */
    public static final String COLUMN_OWNER_EMAIL = COLUMN_EMAIL;
    /**
     * The Constant TABLE_MY_WORKSPACE.
     * Stores Workspaces. A Workspaces is identified by its name and the owner's (user) email.
     * Workspace also has:
     * - quota: defines the size of Workspace.
     * - privacy (is_private attribute): defines the visibility of the workspace.
     * - directory: defines the directory where the workspace is stored.
     */
    public static final String TABLE_MY_WORKSPACE = "my_workspace";
    /**
     * The Constant COLUMN_NAME.
     */
    public static final String COLUMN_NAME = "name";

    /**
     * The Constant COLUMN_WS_NAME.
     */
    public static final String COLUMN_WS_NAME = COLUMN_NAME;

    /**
     * The Constant COLUMN_QUOTA.
     */
    public static final String COLUMN_QUOTA = "quota";

    /**
     * The Constant COLUMN_IS_PRIVATE.
     */
    public static final String COLUMN_IS_PRIVATE = "is_private";

    /**
     * The Constant COLUMN_DIRECTORY.
     */
    public static final String COLUMN_DIRECTORY = "directory";
    // table creation sql statement
    /**
     * The Constant DATABASE_CREATE_MY_WORKSPACE_TABLE.
     */
    private static final String DATABASE_CREATE_MY_WORKSPACE_TABLE = "create table "
            + TABLE_MY_WORKSPACE + "(" + COLUMN_NAME + " TEXT," + COLUMN_OWNER_EMAIL + " TEXT,"
            + COLUMN_QUOTA + " INTEGER," + COLUMN_IS_PRIVATE + " INTEGER," + COLUMN_DIRECTORY
            + " TEXT," + "PRIMARY KEY (" + COLUMN_NAME + "," + COLUMN_OWNER_EMAIL + ")" + ");";

    //-TABLE PEOPLE_WS_ACCESS DETAILS:

    /**
     * The Constant TABLE_PEOPLE_WS_ACCESS.
     * Stores Foreign Users (the users who are not the owner of the Workspace) Workspace access relationship.
     * A Workspace Access is identified by the Workspace's primitive key's and the foreign User's private key.
     */
    public static final String TABLE_PEOPLE_WS_ACCESS = "people_ws_access";

    /**
     * The Constant COLUMN_WS_OWNER_EMAIL.
     */
    public static final String COLUMN_WS_OWNER_EMAIL = "owner_email";

    /**
     * The Constant COLUMN_FOREIGN_EMAIL.
     */
    public static final String COLUMN_FOREIGN_EMAIL = "foreign_email";
    // table creation sql statement
    /**
     * The Constant DATABASE_CREATE_PEOPLE_WS_ACCESS_TABLE.
     */
    private static final String DATABASE_CREATE_PEOPLE_WS_ACCESS_TABLE = "create table "
            + TABLE_PEOPLE_WS_ACCESS + "(" + COLUMN_WS_NAME + " TEXT," + COLUMN_WS_OWNER_EMAIL
            + " TEXT," + COLUMN_FOREIGN_EMAIL + " TEXT," + "PRIMARY KEY (" + COLUMN_WS_NAME + ","
            + COLUMN_WS_OWNER_EMAIL + "," + COLUMN_FOREIGN_EMAIL + ")" + ");";

    //-TABLE PEOPLE_WS_ACCESS_REQUEST DETAILS:

    /**
     * The Constant TABLE_PEOPLE_WS_ACCESS_REQUEST.
     * Stores Foreign Users (the users who are not the owner of the Workspace) Workspace access relationship.
     * A Workspace Access is identified by the Workspace's primitive key's and the foreign User's private key.
     */
    public static final String TABLE_PEOPLE_WS_ACCESS_REQUEST = "ws_request_list";

    /**
     * The Constant COLUMN_WS_OWNER_EMAIL.
     */
    public static final String COLUMN_REQUEST_WS_OWNER_EMAIL = "owner_email";

    /**
     * The Constant COLUMN_FOREIGN_EMAIL.
     */
    public static final String COLUMN_REQUEST_FOREIGN_EMAIL = "foreign_email";
    // table creation sql statement
    /**
     * The Constant DATABASE_CREATE_PEOPLE_WS_ACCESS_REQUEST_TABLE.
     */
    private static final String DATABASE_CREATE_PEOPLE_WS_ACCESS_REQUEST_TABLE = "create table "
            + TABLE_PEOPLE_WS_ACCESS_REQUEST + "(" + COLUMN_WS_NAME + " TEXT," + COLUMN_REQUEST_WS_OWNER_EMAIL
            + " TEXT," + COLUMN_REQUEST_FOREIGN_EMAIL + " TEXT," + "PRIMARY KEY (" + COLUMN_WS_NAME + ","
            + COLUMN_REQUEST_WS_OWNER_EMAIL + "," + COLUMN_REQUEST_FOREIGN_EMAIL + ")" + ");";


    //DATABASE ATTRIBUTE DETAILS:
    /**
     * The Constant DATABASE_NAME.
     */
    private static final String DATABASE_NAME = "AirDeskDB.db";

    /**
     * The Constant DATABASE_VERSION.
     */
    private static final int DATABASE_VERSION = 7;

    /**
     * Instantiates a new my sq lite helper.
     *
     * @param context the context
     */
    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * On create.
     *
     * @param database the database
     */
    @Override
    public synchronized void onCreate(SQLiteDatabase database) {
        Log.w(MySQLiteHelper.class.getName(), "Creating tables");
        database.execSQL(DATABASE_CREATE_USER_TABLE);
        database.execSQL(DATABASE_CREATE_MY_WORKSPACE_TABLE);
        database.execSQL(DATABASE_CREATE_PEOPLE_WS_ACCESS_TABLE);
        database.execSQL(DATABASE_CREATE_PEOPLE_WS_ACCESS_REQUEST_TABLE);
    }

    /**
     * On upgrade.
     *
     * @param db         the db
     * @param oldVersion the old version
     * @param newVersion the new version
     */
    @Override
    public synchronized void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion
                + " to " + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MY_WORKSPACE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PEOPLE_WS_ACCESS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PEOPLE_WS_ACCESS_REQUEST);
        onCreate(db);
    }

}
