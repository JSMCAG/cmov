package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files.task.local;

/**
 * Created by Group 7 on 07-Apr-15.
 */

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.DeleteFileService;

/**
 * The Class DeleteFileAsyncTask.
 * this task deletes the file from the FileStorage.
 */
public class DeleteFileAsyncTask extends
        AsyncTask<String, Void, Void> {

    /**
     * The service.
     */
    private DeleteFileService service;

    /**
     * The file name.
     */
    private String fileName;

    /**
     * The folder absolute path.
     */
    private String folderAbsolutePath;

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * The message.
     */
    private String message;

    /**
     * The context.
     */
    private Context context;

    /**
     * Instantiates a new delete file async task.
     *
     * @param c                  the c
     * @param activity           the activity
     * @param folderAbsolutePath the folder absolute path
     * @param fileName           the file name
     */
    public DeleteFileAsyncTask(Context c,
                               Activity activity,
                               String folderAbsolutePath,
                               String fileName) {
        super();
        this.service = new DeleteFileService(c);
        this.fileName = fileName;
        this.folderAbsolutePath = folderAbsolutePath;
        this.activity = activity;
        this.context = c;
    }

    /**
     * On pre execute.
     */
    @Override
    protected void onPreExecute() {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, "Deleting file: Please wait.", duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(String... params) {

        service.run(folderAbsolutePath, fileName);
        message = "Deleted File Successfully";
        return null;
    }

    /**
     * On post execute.
     *
     * @param aVoid the a void
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
        reloadActivity(this.activity);
    }

    /**
     * Reload activity.
     *
     * @param activity the activity
     */
    private void reloadActivity(Activity activity) {
        activity.finish();
        activity.startActivity(activity.getIntent());
    }
}