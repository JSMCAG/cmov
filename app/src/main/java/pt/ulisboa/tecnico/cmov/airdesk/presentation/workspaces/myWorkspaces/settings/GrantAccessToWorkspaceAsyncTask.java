package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.settings;

import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserHasNotRequestedWorkspaceAccessException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceDoesNotExistException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceIsPrivateException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.GrantWorkspaceAccessService;

/**
 * Created by Filipe Apolinário on 05-May-15.
 */
public class GrantAccessToWorkspaceAsyncTask extends
        AsyncTask<String, Void, String> {

    private Context context;

    public GrantAccessToWorkspaceAsyncTask(Context context){
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
        final String WORKSPACE_NAME = params[0];
        final String OWNER_EMAIL = params[1];
        final String FOREIGN_EMAIL = params[2];
        try {
            GrantWorkspaceAccessService service = new GrantWorkspaceAccessService(context);
            service.run(WORKSPACE_NAME, OWNER_EMAIL, FOREIGN_EMAIL);
        } catch (EmptyFieldsException e){
            return "Some fields were empty";
        } catch (WorkspaceDoesNotExistException e){
            return "Workspace " + WORKSPACE_NAME +" does not exist";
        } catch (WorkspaceIsPrivateException e){
            return "Workspace is private";
        } catch (UserHasNotRequestedWorkspaceAccessException e) {
            return "User did not request Workspace Access";
        }
        return "User added Successfully";
    }

    @Override
    protected void onPostExecute(String message) {
        super.onPostExecute(message);

        if(message != null){
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, message, duration);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            if (v != null) {
                v.setGravity(Gravity.CENTER);
            }
            toast.show();
        }
    }
}
