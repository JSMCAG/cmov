package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces;

/**
 * Created by Group 7 on 22-Mar-15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.ViewInitializer;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchUserWorkspacesService;

/**
 * The Class FetchUserWorkspacesAsyncTask.
 */
public class FetchUserWorkspacesAsyncTask extends
        AsyncTask<String, Void, ArrayList<Workspace>> {

    /**
     * The service.
     */
    private FetchUserWorkspacesService service;

    /**
     * The workspaces list.
     */
    private ArrayList<String> workspacesList;

    /**
     * The activity.
     */
    private ActionBarActivity activity;

    {
        this.workspacesList = new ArrayList<>();
    }

    /**
     * Instantiates a new fetch user workspaces async task.
     *
     * @param context  the context
     * @param activity the activity
     */
    public FetchUserWorkspacesAsyncTask(Context context, ActionBarActivity activity) {
        super();
        service = new FetchUserWorkspacesService(context);
        this.activity = activity;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the array list
     */
    @Override
    protected ArrayList<Workspace> doInBackground(String... params) {
        String ownerEmail = params[0];

        service.run(ownerEmail);
        return service.getResult();
    }

    /**
     * On post execute.
     *
     * @param wsList the ws list
     */
    @Override
    protected void onPostExecute(ArrayList<Workspace> wsList) {
        for (Workspace w : wsList) {
            workspacesList.add(w.getName());
        }
        //Initialize my workspace list
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_list_item_1, workspacesList);
        final ListView wsListView = (ListView) activity.findViewById(R.id.MyWSList);
        final AdapterView.OnItemClickListener wsListViewItemClickListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long l) {
                final int position = wsListView.getPositionForView(myView);
                String title = wsListView.getItemAtPosition(position).toString();
                Bundle args = new Bundle();
                args.putString("title", title);
                args.putString("owner_email", SimpleApp.getUserLoggedIn().getEmail());
                showWorkspaceNoticeDialog(args);
            }
        };
        ViewInitializer initializer = new ViewInitializer();
        initializer
                .initializeMyWorkspacesListView(wsListView, adapter, wsListViewItemClickListener);
    }

    /**
     * Show workspace notice dialog.
     *
     * @param args the args
     */
    private void showWorkspaceNoticeDialog(Bundle args) {
        // Create an instance of the dialog fragment and show it
        WorkspaceDialog dialog = new WorkspaceDialog();
        dialog.setArguments(args);
        dialog.show(activity.getSupportFragmentManager(), "NoticeDialogFragment");
    }


}
