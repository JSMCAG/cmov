package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.fileCommands;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ErrorOccurredException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ServiceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.DeleteFileService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 10-May-15.
 */
public class DeleteFileCommand extends Command {
    private String workspaceName;
    private String fileName;

    private String result;

    public DeleteFileCommand(String workspaceName, String fileName) {
        this.workspaceName = workspaceName;
        this.fileName = fileName;
    }

    public void execute() {
        try {
            FetchWorkspaceService workspaceService = new FetchWorkspaceService(SimpleApp.getContext());
            workspaceService.run(SimpleApp.getUserLoggedIn().getEmail(), workspaceName);

            Workspace ws = workspaceService.getResult();

            DeleteFileService service = new DeleteFileService(SimpleApp.getContext());
            service.run(ws.getDirectory(), fileName);
            result = "Successfully deleted file.";
        } catch (EmptyFieldsException e) {
            result = "Some fields were empty.";
        } catch (ErrorOccurredException e) {
            result = "An error occurred.";
        } catch (ServiceException e) {
            result = "An error ocurred.";
        }
    }

    public String getResult() {
        return result;
    }
}
