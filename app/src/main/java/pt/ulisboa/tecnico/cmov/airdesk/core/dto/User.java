package pt.ulisboa.tecnico.cmov.airdesk.core.dto;

/**
 * Created by Group 7 on 13-Mar-15.
 */

/**
 * The Class User.
 * The main porpoise of this class is to function as a Data Transfer Object between layers.
 */
public class User extends DTO {

    /**
     * The email.
     */
    private String email;

    /**
     * Instantiates a new user.
     *
     * @param email the email
     */
    public User(String email) {
        super();
        this.email = email;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }
}
