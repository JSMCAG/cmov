package pt.ulisboa.tecnico.cmov.airdesk.core.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import pt.ulisboa.tecnico.cmov.airdesk.core.dto.User;

/**
 * The Class UserDataSource.
 * The main porpoise of this class is to function as a handler for manipulating User table.
 *
 * @author Group 7
 * @since 13-Mar-15.
 */
public class UserDataSource {

    // Database fields
    /**
     * The database.
     */
    private SQLiteDatabase database;

    /**
     * The db helper.
     */
    private MySQLiteHelper dbHelper;

    /**
     * The all columns.
     */
    private String[] allColumns = {MySQLiteHelper.COLUMN_EMAIL};

    /**
     * Instantiates a new user data source.
     *
     * @param context the context
     */
    public UserDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    /**
     * Open. This method opens a connection to the AirDesk database (required for manipulating the database).
     *
     * @throws SQLException the SQL exception
     */
    public synchronized void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    /**
     * Close. This method closes a connection to the AirDesk database.
     */
    public synchronized void close() {
        dbHelper.close();
    }

    /**
     * Adds the user to database.
     *
     * @param user the user
     */
    private synchronized void addUserToDatabase(User user) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_EMAIL, user.getEmail());
        database.insert(MySQLiteHelper.TABLE_USER, null, values);
    }

    /**
     * Cursor to user.
     * Converts a database tuple (Cursor) to Workspace Access Object.
     *
     * @param cursor the cursor
     * @return the user
     */
    private synchronized User cursorToUser(Cursor cursor) {
        String title = cursor.getString(0);
        return new User(title);
    }

    /**
     * Adds the user.
     *
     * @param user the user
     */
    public synchronized void addUser(User user) {
        //search if there is a note with the same title as the note we want to insert. If so remove the old one before adding the new one.
        this.addUserToDatabase(user);
    }

    /**
     * Find user by email.
     *
     * @param title the title
     * @return the user
     */
    public User findUserByEmail(String title) {

        Cursor cursor = database.query(MySQLiteHelper.TABLE_USER, allColumns,
                MySQLiteHelper.COLUMN_EMAIL + "=?", new String[]{title}, null, null, null, null);

        User u = null;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            u = cursorToUser(cursor);
        }
        cursor.close();

        return u;
    }

}
