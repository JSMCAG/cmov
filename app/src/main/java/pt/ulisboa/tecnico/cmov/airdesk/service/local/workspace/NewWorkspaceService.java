package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

/**
 * Created by Group 7 on 31-Mar-15.
 */

import android.content.Context;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.QuotaExceededException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ServiceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceAlreadyExistsException;

/**
 * The Class NewWorkspaceService.
 */
public class NewWorkspaceService {

    /**
     * The context.
     */
    private final Context context;

    /**
     * The database my workspace handler.
     */
    private WorkspaceDataSource databaseMyWorkspaceHandler;

    /**
     * Instantiates a new new workspace service.
     *
     * @param context the context
     */
    public NewWorkspaceService(Context context) {
        databaseMyWorkspaceHandler = new WorkspaceDataSource(context);
        this.context = context;
    }

    /**
     * Run.
     *
     * @param name       the name
     * @param ownerEmail the owner email
     * @param quota      the quota
     * @throws ServiceException the service exception
     */
    public void run(String name, String ownerEmail, int quota) throws ServiceException {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {name, ownerEmail};
        if (!verifier.stringVerifier(sParams)) {
            throw new EmptyFieldsException();
        }


        Workspace ws = new Workspace(name, ownerEmail, quota);
        CheckQuotaService service = new CheckQuotaService(context);
        service.run(ws.getDirectory(), quota);
        if (!service.getResult()) {
            throw new QuotaExceededException();
        }

        databaseMyWorkspaceHandler.open();
        if (databaseMyWorkspaceHandler.findWorkspaceByNameAndOwner(name, ownerEmail) != null) {
            throw new WorkspaceAlreadyExistsException();
        }
        databaseMyWorkspaceHandler.addWorkspace(ws);

        databaseMyWorkspaceHandler.close();
    }

    public void run(String name, String ownerEmail, int quota, boolean isPrivate) throws ServiceException {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {name, ownerEmail};
        if (!verifier.stringVerifier(sParams)) {
            throw new EmptyFieldsException();
        }


        Workspace ws = new Workspace(name, ownerEmail, quota, isPrivate);
        CheckQuotaService service = new CheckQuotaService(context);
        service.run(ws.getDirectory(), quota);
        if (!service.getResult()) {
            throw new QuotaExceededException();
        }

        databaseMyWorkspaceHandler.open();
        if (databaseMyWorkspaceHandler.findWorkspaceByNameAndOwner(name, ownerEmail) != null) {
            throw new WorkspaceAlreadyExistsException();
        }
        databaseMyWorkspaceHandler.addWorkspace(ws);

        databaseMyWorkspaceHandler.close();
    }
}
