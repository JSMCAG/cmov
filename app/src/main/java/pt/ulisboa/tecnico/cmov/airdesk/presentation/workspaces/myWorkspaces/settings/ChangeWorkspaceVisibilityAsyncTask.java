package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.settings;

/**
 * Created by Group 7 on 22-Mar-15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.ChangeWorkspaceVisibilityService;

/**
 * The Class ChangeWorkspaceVisibilityAsyncTask.
 * updates Workspace's visibility.
 */
public class ChangeWorkspaceVisibilityAsyncTask extends
        AsyncTask<String, Void, Void> {

    /**
     * The service.
     */
    private ChangeWorkspaceVisibilityService service;

    /**
     * The message.
     */
    private String message;

    /**
     * The context.
     */
    private Context context;

    /**
     * Instantiates a new change workspace visibility async task.
     *
     * @param context the context
     */
    public ChangeWorkspaceVisibilityAsyncTask(Context context) {
        super();
        this.service = new ChangeWorkspaceVisibilityService(context);
        this.context = context;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(String... params) {
        String name = params[0];
        String ownerEmail = params[1];
        boolean isPrivateWorkspace = Boolean.parseBoolean(params[2]);

        service.run(name, ownerEmail, isPrivateWorkspace);
        this.message = "Successfully changed visibility.";
        return null;
    }

    /**
     * On post execute.
     *
     * @param aVoid the a void
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }
}
