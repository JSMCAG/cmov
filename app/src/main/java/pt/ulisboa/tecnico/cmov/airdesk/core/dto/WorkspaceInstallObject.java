package pt.ulisboa.tecnico.cmov.airdesk.core.dto;

import java.util.ArrayList;

/**
 * Created by Filipe Apolinário on 15-May-15.
 */
public class WorkspaceInstallObject extends DTO {
    private Workspace ws;

    private ArrayList<WorkspaceAccess> l;

    private ArrayList<FileDto> filesWs;

    public WorkspaceInstallObject(Workspace ws, ArrayList<WorkspaceAccess> l, ArrayList<FileDto> filesWs) {
        this.ws = ws;
        this.l = l;
        this.filesWs = filesWs;
    }

    public Workspace getWs() {
        return ws;
    }

    public ArrayList<WorkspaceAccess> getL() {
        return l;
    }

    public ArrayList<FileDto> getFilesWs() {
        return filesWs;
    }
}
