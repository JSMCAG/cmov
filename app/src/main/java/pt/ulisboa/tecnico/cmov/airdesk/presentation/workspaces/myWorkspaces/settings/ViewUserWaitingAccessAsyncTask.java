package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.settings;

/**
 * Created by Group 7 on 22-Mar-15.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchRequestsWorkspaceAccessService;


/**
 * The Class ViewPeopleWorkspaceAccessAsyncTask.
 * Fetches the Users in Workspace.
 */
public class ViewUserWaitingAccessAsyncTask extends
        AsyncTask<String, Void, ArrayList<WorkspaceAccess>> {

    /**
     * The service.
     */
    private FetchRequestsWorkspaceAccessService service;

    /**
     * The activity.
     */
    private ActionBarActivity activity;

    /**
     * The WORKSPACE_NAME.
     */
    private String WORKSPACE_NAME;

    /**
     * The email.
     */
    private String email;

    /**
     * Instantiates a new view people workspace access async task.
     *
     * @param context  the context
     * @param activity the activity
     */
    public ViewUserWaitingAccessAsyncTask(Context context, ActionBarActivity activity) {
        super();
        this.service = new FetchRequestsWorkspaceAccessService(context);
        this.activity = activity;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the array list
     */
    @Override
    protected ArrayList<WorkspaceAccess> doInBackground(String... params) {
        this.WORKSPACE_NAME = params[0];
        this.email = params[1];

        service.run(WORKSPACE_NAME, email);

        return service.getResult();
    }

    /**
     * On post execute.
     *
     * @param wsList the ws list
     */
    @Override
    protected void onPostExecute(ArrayList<WorkspaceAccess> wsList) {
        ArrayList<String> list = new ArrayList<>();
        for (WorkspaceAccess ws : wsList) {
            list.add(ws.getEmailWithWorkspaceAccess());
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final AlertDialog dialog;
        builder.setTitle("User waiting for workspace access");

        final ArrayAdapter<String> lAdapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_list_item_1, list);

        final ListView modeList = new ListView(activity);
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                final int position = modeList.getPositionForView(myView);
                String user = modeList.getItemAtPosition(position).toString();
                Bundle args = new Bundle();
                args.putString("workspace", WORKSPACE_NAME);
                args.putString("person", user);
                grantAccessToWorkspaceDialog(args);
            }
        });

        modeList.setAdapter(lAdapter);
        builder.setView(modeList);

        builder.create().show();

    }

    /**
     * Grant access to workspace dialog.
     *
     * @param args the args
     */
    private void grantAccessToWorkspaceDialog(Bundle args) {
        GrantAccessToWorkspaceDialog dialog = new GrantAccessToWorkspaceDialog();
        dialog.setArguments(args);
        dialog.show(activity.getSupportFragmentManager(), "NoticeDialogFragment");
    }

}
