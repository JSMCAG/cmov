package pt.ulisboa.tecnico.cmov.airdesk.service.local.file;

/**
 * Created by Group 7 on 07-Apr-15.
 */

import android.content.Context;

import java.io.File;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.FileStorageHandler;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;

/**
 * The Class DeleteFileService.
 * This service deletes file to FileStorage.
 */
public class DeleteFileService {

    /**
     * The file directory.
     */
    private File fileDirectory;

    /**
     * Instantiates a new delete file service.
     *
     * @param context the context
     */
    public DeleteFileService(Context context) {
        this.fileDirectory = context.getFilesDir();
    }

    /**
     * Run.
     *
     * @param workspaceRelativePAth the workspace relative p ath
     * @param fileName              the file name
     */
    public void run(String workspaceRelativePAth, String fileName) {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {workspaceRelativePAth, fileName};
        if (!verifier.stringVerifier(sParams)) {
            throw new EmptyFieldsException();
        }

        String absolutePath = fileDirectory.getAbsolutePath();

        FileStorageHandler handler = new FileStorageHandler();
        String wsAbsPath = absolutePath + File.separator + workspaceRelativePAth;
        if (!handler.doesDirectoryExist(wsAbsPath)) {
            handler.createDirectory(wsAbsPath);
        }

        handler.deleteFile(wsAbsPath + File.separator + fileName);
    }
}
