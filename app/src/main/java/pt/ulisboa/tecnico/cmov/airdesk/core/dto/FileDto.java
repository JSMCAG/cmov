package pt.ulisboa.tecnico.cmov.airdesk.core.dto;

/**
 * Created by Filipe Apolinário on 15-May-15.
 */
public class FileDto extends DTO {
    String fileName;
    String content;

    public FileDto(String fileName, String content) {
        this.fileName = fileName;
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public String getContent() {
        return content;
    }
}
