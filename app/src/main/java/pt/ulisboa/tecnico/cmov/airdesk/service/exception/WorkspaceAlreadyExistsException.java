package pt.ulisboa.tecnico.cmov.airdesk.service.exception;

/**
 * Created by Group 7 on 07-Apr-15.
 */


/**
 * The Class WorkspaceAlreadyExistsException.
 * This exception is raised when a service is trying to create a new Workspace that already exists.
 */
public class WorkspaceAlreadyExistsException extends
        ServiceException {
}
